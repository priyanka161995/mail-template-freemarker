 <#if payment_info["partial_wallet"]??>

 <#if payment_info["other_payment_mode"].payment_type != "NB">

<tr>

								<td>

									<table width="100%" border="0" cellspacing="0" cellpadding="0">

										<tbody>

											<tr>

                        <td style="text-align:center;padding-right:5px;width:48%;padding-bottom:5px;">

                           <#if (payment_info["no_of_payments"][0]["logo_text"])?? >

                            <span style="font-size: 18px;font-weight: regular;color: #000000;text-align: center;">${payment_info["no_of_payments"][0]["logo_text"]}</span>

                           <#else> 

			                      <img src="https://${hostname}/images/mailers/ ${payment_info["no_of_payments"][0]["logo"]}_logo.png" style="padding:10px 0;font-size:18px;color:#000000;" alt=" ${payment_info["no_of_payments"][0]["payment_type"]}">

                          </#if> 

                        </td>

												<td style="text-align:left;color:#999999;padding-right:5px;width:2%;padding-bottom:5px;">

													<span>+</span>

												</td>

												<td style="text-align:left;padding-left:5px;width:50%;padding-bottom:5px;">

                           <#if (payment_info["no_of_payments"][1]["logo_text"]).present? >

                            <span style="font-size: 18px;font-weight: regular;color: #000000;text-align: center;">${payment_info["no_of_payments"][1]["logo_text"]}</span>

                           <#else> 

                             <img src="https://${hostname}/images/mailers/${payment_info["no_of_payments"][1]["logo"]}_logo.png" style="padding:10px 0;font-size:18px;color:#000000;" alt="${payment_info["no_of_payments"][1]["payment_type"]}">

                          </#if> 

                         </td>

											</tr>

										</tbody>

									</table>

								</td>

							</tr>

							<tr>

 <#elseif payment_info["other_payment_mode"].payment_type?? && payment_info["other_payment_mode"].payment_type == "NB">

  <tr>

								<td>

								<img src="https://${hostname}/images/mailers/ ${payment_info["no_of_payments"][1]["logo"]_logo.png" style="padding:10px 0;font-size:18px;color:#000000;" alt="${payment_info["no_of_payments][1]["payment_type"]}">	

								</td>

							</tr>

							<tr>

								<td style="border-top:1px solid #eeeeee;padding-bottom:10px;">

									<div style="color:#999999;font-size:10px;text-transform:uppercase;margin:10px 0 0px 0;">Netbanking through</div>

									<div style="color:#000000;font-size:18px;margin:0px;font-weight:bold;">${payment_info["no_of_payments"][0]["payment_type_id"]}</div>

								</td>

							</tr>

</#if>

 <#else>

 <tr>

    <#if payment_info["logo_text"]?? >

    <span style="font-size: 18px;font-weight: regular;color: #000000;text-align: center;">${payment_info["logo_text"]}</span>

<#elseif payment_info["logo"] >

								<td><img src="https://${hostname}/images/mailers/${payment_info["logo"]}_logo.png" style="padding:10px 0;font-size:18px;color:#000000;" alt=" ${payment_info["payment_type"]}">

</td>

  <#else> 

<td>

        <span style="font-size:18px;font-weight:bold;text-transform:uppercase;color:#000000;">${payment_info["payment_type"]}</style>

</td>

      </#if> 

			</tr>

</#if>

