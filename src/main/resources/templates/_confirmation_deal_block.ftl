<tr>

  <td height="20px" colspan="2">&nbsp;</td>

</tr>



<#if !trip_corp_booking>

<tr>

  <td colspan="2" style="background-color:#f4f4f4;">

    <img src="https://${hostname}/images/mailers/deal_ribbon.gif" width="575" height="30" alt="Exciting deal just for your" style=""/>

  </td>

</tr>

<tr>

  <td colspan="2" style="text-align:center;padding:20px 0 30px 0;background-color:#f4f4f4;" id="flightUpsell">

    <a href='${banner_link}' target='_blank' style="display:block;">

    	<img src='${banner_image}' width='575' height='160' alt=''/>

    </a>

  </td>

</tr>

<tr>

  <td style="background-color:#f4f4f4;border-top:1px dashed #cccccc;" colspan="2" height="2px"></td>

</tr>

</#if>
