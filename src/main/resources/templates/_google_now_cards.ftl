<#if google_card_enabled?? && google_card_enabled && google_now_card_script??>

<script type="application/ld+json">

  ${google_now_card_script_html_safe}

</script>

<script type="application/ld+json">

{

  "context": "http://schema.org",

  "type": "EmailMessage",

  "action": {

    "type": "ViewAction",

    "url": "https://www.cleartrip.com/account/trips/${trip.trip_ref}",

    "name": "View itinerary"

  },

  "description": "View you itinerary"

}

</script>

</#if>