<#if payment_info["payments"]??>

<#-- include "src/main/resources/templates/_multiple_payment_receipt.ftl" -->
<#else>

<div style="height:18px;width:272px"><img src="https://${hostname}/images/mailers/sticky.gif" height="18" width="272" style="vertical-align:top"/></div><div style="padding:10px;border-right:1px solid #eeeeee;border-left:1px solid #eeeeee;">

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:center;">

  <tr>

    <td style="font-size:10px;text-transform:uppercase;color:#666666;">Payment Receipt</td>

  </tr>

   <#if payment_type_key == "WT" || (payment_info["partial_wallet"]?? && payment_info["partial_wallet"])>

<#include "src/main/resources/templates/_wallet_payment_receipt.ftl">
   <#else>

  <tr>

    <td style="padding-bottom:5px;">

       <#if payment_info["logo_text"]?? >

        <span style="font-size: 18px;font-weight: regular;color: #000000;text-align: center;">${payment_info["logo_text"]}</span>

      <#elseif payment_info["logo"]?? >

      <img src="https://${hostname}/images/mailers/${payment_info["logo"]}_logo.png" style="padding:10px 0;font-size:18px;color:#000000;" alt=<#if payment_info["payment_type"] ==  payment_type["TW"]>  ${payment_info["logo"]} <#else> ${payment_info["payment_type"]} </#if>>

       <#else> 

        <span style="font-size:18px;font-weight:bold;text-transform:uppercase;color:#000000;"><#if payment_info["payment_type"]??>${payment_info["payment_type"]} </#if></style>

      </#if> 

    </td>

  </tr>

  <tr>

    <td style="border-top:1px solid #eeeeee;padding-bottom:10px;">

      <div style="color:#999999;font-size:10px;text-transform:uppercase;margin:10px 0 0px 0;"><#if payment_info["payment_type"]??> ${payment_info["payment_type"]} </#if> </div>

      <div style="color:#000000;font-size:18px;margin:0px;font-weight:bold;"><#if payment_info["payment_type_id"]??>${payment_info["payment_type_id"]} </#if></div>

    </td>

  </tr>

 </#if>



  <tr>

    <td style="border-top:1px solid #eeeeee;padding-bottom:10px;">

      <div style="color:#999999;font-size:10px;text-transform:uppercase;margin:10px 0 0px 0;">Total charge</div>

      <div style="color:#000000;font-size:18px;margin:0px;font-weight:bold;">${format_total}</div>

    </td>

  </tr>

  <tr>

    <td style="border-top:1px solid #eeeeee;">

      <div style="color:#999999;font-size:10px;text-transform:uppercase;margin:10px 0 0px 0;"> Fare breakup</div>

       <#if has_free_cancellation>

       <div style="color:#000000;font-size:18px;margin:10px 0px;font-size:12px;">	

      <#else>

       <div style="color:#000000;font-size:18px;margin:10px 0px;font-size:13px;">

      </#if>

        <table width="100%" border="0" cellspacing="0" cellpadding="0">

<#list payment_info["pricing_info"]  as   pricing_info>
          <tr>

            <td style="text-align:right;color:#999999;vertical-align:top;padding-right:5px;width:50%;padding-bottom:5px;">${pricing_info[0]}</td>

            <td style="text-align:left;color:#000000;vertical-align:top;padding-left:5px;width:50%;padding-bottom:5px;">${pricing_info[1]}</td> <#--GeneralHelpers::format_currency(pricing_info[1].abs, 0 ,currency)} -->

          </tr>

          </#list> 

	   <#if additional_breakup??>

<#list additional_breakup as  add_bearkup>
	  <tr>

            <td style="text-align:right;color:#999999;vertical-align:top;padding-right:5px;width:50%;padding-bottom:5px;">${add_bearkup[0]}</td>

            <td style="text-align:left;color:#000000;vertical-align:top;padding-left:5px;width:50%;padding-bottom:5px;">${add_bearkup[1]}</td> <#--${GeneralHelpers::format_currency(add_bearkup[1].abs, 0 ,currency)} -->

          </tr>

	  </#list> 

	  </#if> 

          <tr>

            <td style="text-align:right;color:#999999;vertical-align:top;padding-right:5px;padding-bottom:5px;">Total</td>

            <td style="text-align:left;color:#000000;vertical-align:top;padding-left:5px;padding-bottom:5px;">${format_total}</td>

          </tr>

          <#if payment_info["hotel_part_pay"]?? && payment_info["hotel_part_pay"] == true >

          <tr>

            <td style="text-align:right;color:#999999;vertical-align:top;padding-right:5px;padding-bottom:5px;">Part payment</td>

            <td style="text-align:left;color:#000000;vertical-align:top;padding-left:5px;padding-bottom:5px;"> <#if part_payment??> ${part_payment} </#if></td>

          </tr>

          <tr>

            <td style="text-align:right;color:#999999;vertical-align:top;padding-right:5px;padding-bottom:5px;">Balance paid</td>

            <td style="text-align:left;color:#000000;vertical-align:top;padding-left:5px;padding-bottom:5px;">${balance_amount}</td>

          </tr>

          </#if>

        </table>

      </div>

    </td>

  </tr>

  </table>

</div>

<div style="height:18px;width:272px"><img src="https://${hostname}/images/mailers/perforation.gif" height="18" width="272" /></div>

</#if>

