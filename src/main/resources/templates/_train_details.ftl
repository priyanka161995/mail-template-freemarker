<tr>

  <td style="font-size:13px;color:#666666;font-weight:bold;letter-spacing:-0.2px;">Just booked</td>

</tr>

<#list  segments as segment >

<tr>

  <td>

    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-top:10px;padding-bottom:10px;">

      <tr>

        <td>

          <table style="padding-bottom:10px;" width="100%" border="0" cellpadding="0" cellspacing="0">

            <tbody><tr>

              <td width="37px;"><img src="https://www.cleartrip.com/images/logos/rail_logo.png" title="IRCTC" alt="IRCTC" style="font-size:10px;color:#999999;vertical-align:top;line-height:13px;" height="30px;" width="30px;"></td>

              <td style="font-size:12px;color:#444444;vertical-align:top;line-height:13px;" colspan="2">${segment.train_name} <br />= ${segment.train_number}</td>

            </tr>

          </tbody></table>

        </td>

      </tr>

    </table>

  </td>

</tr>

<tr>

  <td>

    <table style="border-bottom:1px solid #eeeeee;padding-bottom:20px;" width="100%" border="0" cellpadding="0" cellspacing="0">

      <tbody><tr>

        <td style="font-size:13px;color:#000000;"><#if segment.departure_city_name??>${segment.departure_city_name}</#if>  &rarr; <#if segment.arrival_city_name??> ${segment.arrival_city_name} </#if> <br><div style="font-size:11px;color:#999999;margin-top:3px;">= ${segment.departure_date_time?datetime.iso?string("EEE, dd MM yyyy")}</div></td>

        <td style="font-size:13px;color:#000000; text-align: left;">${segment.departure_date_time?datetime.iso?string("HH:mm")}  &mdash; ${segment.arrival_date_time?datetime.iso?string("HH:mm")}  <br> <div style="font-size:11px;color:#999999;margin-top:3px;">= ${(segment.duration/(60*60))?floor}h, ${((segment.duration/(60*60)-((segment.duration/(60*60))?floor))*60)?round}m</div></td>

        <#-- td style="font-size:13px;color:#000000; text-align: left;">${segment.departure_date_time.strftime("%H:%M")  &mdash; = segment.arrival_date_time.strftime("%H:%M")  <br> <div style="font-size:11px;color:#999999;margin-top:3px;">= DateHelpers.pretty_print_duration(segment.duration).gsub("min", "m").split(", ").join(" ").strip}</div></td> -->

      </tr>

    </tbody></table>

  </td>

</tr>

</#list> 

