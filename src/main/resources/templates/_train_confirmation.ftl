<tr>

  <td style="font-size:22px;color:#000000;font-weight:bold;" bgcolor="#ffffff">

    <h1 style="color:#000000; padding-left:20px; padding-top:0px; font-size:24px;">

      <#if trip_unconfirmed_status >

        <img height="25px" width="191px" src="https://${hostname}/images/mailers/train_waitlisted_title.png" alt="Ticket attached"/>

       <#else> 

        <img height="33px" width="203px" src="https://${hostname}/images/mailers/train_confirmed_title.png" alt="Train confirmed"/>

      </#if>

    </h1>

  </td>

  <td style="text-align:right; padding-right:20px; padding-top:15px" bgcolor="#ffffff">

    <h2>

      <img width="137px" height="45px" alt="Cleartrip" src="https://${hostname}/images/mailers/cleartrip_logo.png"/>

    </h2>

  </td>

</tr>

<tr>

  <td colspan="2" style="padding:0 30px 20px 25px;font-size:18px;color:#000000;">

    <#if trip_unconfirmed_status >

      <p style="margin-bottom:10px;">Hi ${trip.contact_detail.first_name},</p>

       <#--trb = train_booking_infos.detect{|x| x.confirmation_status != "CNF" && x.booking_status == "P"} -->

        <p style="margin:10px 0;line-height:23px;">Your ${trip.trip_name} train ticket is booked, however your PNR status is <strong>= <#if trb_status?? >${trb_status }</#if></strong>. The ticket is attached along with this mail.</p>

        <p style="font-size:12px;color:#666666;padding-top:10px;margin-top:10px;">Your Trip ID is <a href="${account_links['trip_details_page']} ?utm_source=train&utm_medium=email&utm_campaign=confirmation_email"><strong> ${trip.trip_ref}</strong></a>, you should use this in all communication with Cleartrip.</p>

      
    <#else>

      <p style="margin-bottom:10px;">Hi ${trip.contact_detail.first_name},</p>

      <p style="margin:10px 0;line-height:23px;">Your ${trip.trip_name} train ticket is confirmed and attached with this email.</p>

        <p style="font-size:12px;color:#666666;padding-top:10px;margin-top:10px;">Your Cleartrip Trip ID is <a href="${account_links['trip_details_page']} ?utm_source=train&utm_medium=email&utm_campaign=confirmation_email"><strong> ${trip.trip_ref}</strong></a>, you should use this in all communication with Cleartrip.</p>

       <#if (train_booking_info_exists) >

        <p style="font-size:15px;color:#000000;padding-top:10px;margin-top:10px;">Your PNR no. is <a href="#" title="Check your PNR status online"><strong>${train_booking_infos?first.pnr_number}</strong></a></p>


      </#if> 

    </#if>

  </td>

</tr>

