<#assign trip_type = 'train' >    


<table style="background-color:#f4f4f4;" width="100%" cellpadding="3" cellspacing="0">

 <#if p?? && p_confirmed_cleartrip_user >

  <#assign cells = 0 > 

  <tr>

   <#if cells < 2 && upsells?seq_contains("Cab") >

    <#assign cabs_facility = false >
    

     <#if cabs_facility >

      <!-- Book a Cab -->

      <td width="35px" style="vertical-align:top;"><img src="https://${hostname}/images/mailers/cab_icon.png" height="34px" width="34px" alt="Book a cab" style="font-size:11px;color:#999999;"></td>

      <td width="220px" style="vertical-align:top;margin-right:10px;padding:0 15px 0 5px;font-size:15px; font-weight:bold; text-shadow:0 1px 0 #ffffff; color:#333333;">Book a cab <span style="font-size:11px;color:#999999;display:block;margin:2px 0 10px 0;font-weight:normal;">Book a cab to your departure airport. Service available in select cities.</span> <a href="https://${hostname}/account/trips/${trip.trip_ref}?utm_source== ${trip_type}&utm_medium=email&utm_campaign=confirmation_email" style="font-size:13px;font-weight:normal;">Book a cab &rarr;</a></td>

       <#assign cells = cells + 1> 

     <#else> 

      <!-- Book on Mobile-->

      <td width="35px" style="vertical-align:top;"><img src="https://${hostname}/images/mailers/mobile_icon.png" height="34px" width="34px" alt="Book on the go" style="font-size:11px;color:#999999;"></td>

      <td width="220px" style="vertical-align:top;margin-right:10px;padding:0 15px 0 5px;font-size:15px; font-weight:bold; text-shadow:0 1px 0 #ffffff; color:#333333;">Book on the go <span style="font-size:11px;color:#999999;display:block;margin:2px 0 10px 0;font-weight:normal;">Book flights, hotels && trains on the move with Cleartrip Mobile. </span> <a href="https://${hostname}/mobile?utm_source== ${trip_type}&utm_medium=email&utm_campaign=confirmation_email" style="font-size:13px;font-weight:normal;">Go mobile &rarr;</a></td>

       <#assign cells = cells + 1> 

    </#if> 

  </#if> 



   <#if cells < 2 && upsells?seq_contains("Expressway") >

     <#if !p_express_checkout_enabled >

      <!-- Express Way -->

      <td width="35px" style="vertical-align:top;"><img src="https://${hostname}/images/mailers/expressway_icon.png" height="34px" width="34px" alt="Expressway" style="font-size:11px;color:#999999;"></td>

      <td width="220px" style="vertical-align:top;margin-right:10px;padding:0 15px 0 5px;font-size:15px; font-weight:bold; text-shadow:0 1px 0 #ffffff; color:#333333;">Expressway <span style="font-size:11px;color:#999999;display:block;margin:2px 0 10px 0;font-weight:normal;">Save your card details once && book flights & hotels with 1 click thereafter.</span> <a href="https://${hostname}/expressway?utm_source==${trip_type}&utm_medium=email&utm_campaign=confirmation_email" style="font-size:13px;font-weight:normal;">Start by adding a card &rarr;</a></td>

       <#assign cells = cells + 1> 

     <#else> 

      <!-- Book Fare Alerts -->

      <td width="35px" style="vertical-align:top;"><img src="https://${hostname}/images/mailers/fare_alert_icon.png" height="34px" width="34px" alt="Hotel guarantee" style="font-size:11px;color:#999999;"></td>

      <td width="220px" style="vertical-align:top;margin-right:10px;padding:0 15px 0 5px;font-size:15px; font-weight:bold; text-shadow:0 1px 0 #ffffff; color:#333333;">Fare Alerts <span style="font-size:11px;color:#999999;display:block;margin:2px 0 10px 0;font-weight:normal;">Track fare history, get notifications in your inbox && save BIG. </span> <a href="https://${hostname}/alerts?utm_source==${trip_type}&utm_medium=email&utm_campaign=confirmation_email" style="font-size:13px;font-weight:normal;">Set Alert &rarr;</a></td>

       <#assign cells = cells + 1> 

    </#if> 

  </#if> 



   <#if cells < 2 && upsells?seq_contains("Mobile") >

    <!-- Book on Mobile-->

    <td width="35px" style="vertical-align:top;"><img src="https://${hostname}/images/mailers/mobile_icon.png" height="34px" width="34px" alt="Book on the go" style="font-size:11px;color:#999999;"></td>

    <td width="220px" style="vertical-align:top;margin-right:10px;padding:0 15px 0 5px;font-size:15px; font-weight:bold; text-shadow:0 1px 0 #ffffff; color:#333333;">Book on the go <span style="font-size:11px;color:#999999;display:block;margin:2px 0 10px 0;font-weight:normal;">Book flights, hotels and trains on the move with Cleartrip Mobile. </span> <a href="https://${hostname}/mobile?utm_source==${trip_type}&utm_medium=email&utm_campaign=confirmation_email" style="font-size:13px;font-weight:normal;">Go mobile &rarr;</a></td>

     <#assign cells = cells + 1> 

  </#if> 



   <#if cells < 2 && upsells?seq_contains("FareAlert") >

    <!-- Book Fare Alerts -->

    <td width="35px" style="vertical-align:top;"><img src="https://${hostname}/images/mailers/fare_alert_icon.png" height="34px" width="34px" alt="Hotel guarantee" style="font-size:11px;color:#999999;"></td>

    <td width="220px" style="vertical-align:top;margin-right:10px;padding:0 15px 0 5px;font-size:15px; font-weight:bold; text-shadow:0 1px 0 #ffffff; color:#333333;">Fare Alerts <span style="font-size:11px;color:#999999;display:block;margin:2px 0 10px 0;font-weight:normal;">Track fare history, get notifications in your inbox && save BIG. </span> <a href="https://${hostname}/alerts?utm_source=$[trip_type}&utm_medium=email&utm_campaign=confirmation_email" style="font-size:13px;font-weight:normal;">Set Alert &rarr;</a></td>

     <#assign cells = cells + 1> 

  </#if> 



   <#if cells < 2 && upsells?seq_contains("Quickeys") >

    <!-- Quickey -->

    <td width="35px" style="vertical-align:top;"><img src="https://${hostname}/images/mailers/quickey_icon.png" height="34px" width="34px" alt="Expressway" style="font-size:11px;color:#999999;"></td>

    <td width="220px" style="vertical-align:top;margin-right:10px;padding:0 15px 0 5px;font-size:15px; font-weight:bold; text-shadow:0 1px 0 #ffffff; color:#333333;">Quickeys <span style="font-size:11px;color:#999999;display:block;margin:2px 0 10px 0;font-weight:normal;">Get exclusive deals on hotel rooms for same day || next day check-in.</span> <a href="https://${hostname}/quickeys?utm_source==${trip_type}&utm_medium=email&utm_campaign=confirmation_email" style="font-size:13px;font-weight:normal;">Find Quickeys &rarr;</a></td>

     <#assign cells = cells + 1> 

  </#if> 

  </tr>

 <#else> 




  <#if !trip_corp_booking>

    <tr>

      <td colspan="8" style="padding:0px 0 20px 0;text-align:center;font-size:24px;color:#000;text-shadow:0 1px 1px #ffffff;font-weight:bold; ">

        It's time to get your own Cleartrip account

      </td>

    </tr>

    <tr>

      <td style="vertical-align:top" width="38px"><img src="https://${hostname}/images/mailers/print_icon.png" height="34px" width="34px" alt="Print tickets" style="font-size:10px;color:#999999;"></td>

      <td style="vertical-align:top;font-size:12px;color:#666666;" width="80px">Print and <br/>email tickets</td>

      <td style="vertical-align:top" width="38px"><img src="https://${hostname}/images/mailers/email_icon.png" height="34px" width="34px" alt="Email itinerary" style="font-size:10px;color:#999999;"></td>

      <td style="vertical-align:top;font-size:12px;color:#666666;" width="80px">Email and <br/>SMS itinerary</td>

      <td style="vertical-align:top" width="38px"><img src="https://${hostname}/images/mailers/account_icon.png" height="34px" width="34px" alt="Store travellers" style="font-size:10px;color:#999999;"></td>

      <td style="vertical-align:top;font-size:12px;color:#666666;" width="80px">Store traveller information</td>

      <td style="vertical-align:top" width="38px"><img src="https://${hostname}/images/mailers/cancellation_icon.png" height="34px" width="34px" alt="Online cancellations" style="font-size:10px;color:#999999;"></td>

      <td style="vertical-align:top;font-size:12px;color:#666666;" width="80px">Easy online cancellations</td>

    </tr>

    <tr>

      <td colspan="8" style="text-align:center;padding:20px 0 0 0;">

         <#if hide_button> 

        <a href="${activation_url}" style="padding-top:5px;"><img src="https://${hostname}/images/mailers/get_account_button.png" alt="Get it now" border="0"></a>

        </#if> 

      </td>

    </tr>

  <#else>

    <tr><td style="background-color:#f4f4f4;border-top:1px dashed #cccccc;" colspan="2" height="2px;"></td></tr>

    <tr>

      <td style="background-color:#f4f4f4;padding:20px 25px 10px 25px;text-align:left;letter-spacing:-0.2px;" colspan="2">

        <table>

          <tr>

            <td width="35px;" style="vertical-align:top;"><img src="https://${hostname}/images/account/account_icon.png" height="34px;" width="34px;" alt="Personal trip details" style="font-size:11px;color:#999999;"></td>

            <#if !hide_button>

              <#assign account_url = activation_url>

            <#else>

              <#assign account_url = "https://${hostname}/signin?trip_ref=${trip.trip_ref}" >

            </#if>

            <td style="vertical-align:top;margin-right:10px;padding:0 15px 0 5px;font-size:15px; font-weight:bold; text-shadow:0 1px 0 #ffffff; color:#333333;">Send to personal account <span style="font-size:12px;color:#999999;display:block;margin:2px 0 10px 0;font-weight:normal;">Easily send this trip to your personal Cleartrip Account in a few steps. &nbsp;</span> <a href="${account_url}" style="font-size:13px;font-weight:normal;">Send trip to my account &rarr;</a></td>

            <td width="35px;" style="vertical-align:top;"><img src="https://${hostname}/images/account/mobile_icon.png" height="34px;" width="34px;" alt="Cleartrip App" style="font-size:11px;color:#999999;"></td>

            <td style="vertical-align:top;margin-right:10px;padding:0 10px 0 5px;font-size:15px; font-weight:bold; text-shadow:0 1px 0 #ffffff; color:#333333;">Download the Cleartrip App <span style="font-size:12px;color:#999999;display:block;margin:2px 0 10px 0;font-weight:normal;">Download the free Cleartrip App and carry the ticket with you where-ever you go.</span> <a href="https://${hostname}/m" style="font-size:13px;font-weight:normal;">Download the app &rarr;</a></td>

          </tr>

        </table>

      </td>

    </tr>
 </#if>
 </#if>

</table>

