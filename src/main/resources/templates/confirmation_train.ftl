<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <title>${confirmation_subject}</title>

    <style>

    body {font-family:Arial, Verdana,sans-serif; margin:0; padding:0;}

    h1, h2, h3, h4, h5, h6 { margin:0; padding:0;}

    a { color:#3366cc; outline:none; border:none}

    a img { outline:none; border:none;}

  </style>

</head>

<body style="font-family:Arial, Verdana,sans-serif; margin:0; padding:0;"> <body style="background-color: #666666; padding-top: 10px; padding-bottom: 10px;">

<#if google_card_enabled?? && google_card_enabled && google_now_card_script??>

<script type="application/ld+json">

  ${google_now_card_script_html_safe}

</script>

<script type="application/ld+json">

{

  "context": "http://schema.org",

  "type": "EmailMessage",

  "action": {

    "type": "ViewAction",

    "url": "https://www.cleartrip.com/account/trips/${trip_ref}",

    "name": "View itinerary"

  },

  "description": "View you itinerary"

}

</script>

</#if>

<div style="background:#cccccc; margin:0; width:100%">

    <div style="margin:0px auto; width:575px; padding:30px 0 0px 0;">

        <p style="text-align:center;"><img src="https://${hostname}/images/mailers/thankyou_top.gif" style="text-align:center; padding:10px 0 0 0; font-size:24px;color:#9f9f9f;" width="437px" height="38px" alt="Thank you for booking with Cleartrip"></p>

        <div style="height:10px;-webkit-border-radius:10px 10px 0 0;border-radius:10px 10px 0 0;background-color:#ffffff;"></div>

        <div style="background-color:#ffffff;">

            <table width="100%" border="0" cellspacing="0" cellpadding="0">

                <tr>

                    <#if trip_fully_confirmed || trip_booking_status == "Q" || trip_booking_status == "K" || (trip_has_failed_parts && trip_convert_failure_to_success_activated)  >

                    <#if trip_train_booking_exists >

                    <tr>

                        <td style="font-size:22px;color:#000000;font-weight:bold;" bgcolor="#ffffff">

                            <h1 style="color:#000000; padding-left:20px; padding-top:0px; font-size:24px;">

                                <#if trip_unconfirmed_status >

                                <img height="25px" width="191px" src="https://${hostname}/images/mailers/train_waitlisted_title.png" alt="Ticket attached"/>

                                <#else>

                                <img height="33px" width="203px" src="https://${hostname}/images/mailers/train_confirmed_title.png" alt="Train confirmed"/>

                            </#if>

                        </h1>

                    </td>

                    <td style="text-align:right; padding-right:20px; padding-top:15px" bgcolor="#ffffff">

                        <h2>

                            <img width="137px" height="45px" alt="Cleartrip" src="https://${hostname}/images/mailers/cleartrip_logo.png"/>

                        </h2>

                    </td>

                </tr>

                <tr>

                    <td colspan="2" style="padding:0 30px 20px 25px;font-size:18px;color:#000000;">

                        <#if trip_unconfirmed_status >

                        <p style="margin-bottom:10px;">Hi ${firstname},</p>

                        <p style="margin:10px 0;line-height:23px;">Your ${trip_name} train ticket is booked, however your PNR status is <strong>= <#if trb_status?? >${trb_status }</#if></strong>. The ticket is attached along with this mail.</p>

                        <p style="font-size:12px;color:#666666;padding-top:10px;margin-top:10px;">Your Trip ID is <a href="${trip_details_page} ?utm_source=train&utm_medium=email&utm_campaign=confirmation_email"><strong> ${trip_ref}</strong></a>, you should use this in all communication with Cleartrip.</p>


                    <#else>

                    <p style="margin-bottom:10px;">Hi ${firstname},</p>

                    <p style="margin:10px 0;line-height:23px;">Your ${trip_name} train ticket is confirmed and attached with this email.</p>

                    <p style="font-size:12px;color:#666666;padding-top:10px;margin-top:10px;">Your Cleartrip Trip ID is <a href="${trip_details_page} ?utm_source=train&utm_medium=email&utm_campaign=confirmation_email"><strong> ${trip_ref}</strong></a>, you should use this in all communication with Cleartrip.</p>

                    <#if (train_booking_info_exists) >

                    <p style="font-size:15px;color:#000000;padding-top:10px;margin-top:10px;">Your PNR no. is <a href="#" title="Check your PNR status online"><strong>${pnr_number}</strong></a></p>


                </#if>

            </#if>

        </td>

    </tr>


    <#else>

    <tr>

        <td style="font-size:22px;color:#000000;font-weight:bold;" bgcolor="#ffffff">

            <h1 style="color:#000000; padding-left:20px; padding-top:0px; font-size:24px;">

                <img height="25px" width="191px" src="https://${hostname}/mailers/images/train_confirmed_title.png" alt="Ticket attached"/>

            </h1>

        </td>

        <td style="text-align:right; padding-right:20px; padding-top:15px" bgcolor="#ffffff">

            <h2>

                <img width="137px" height="45px" alt="Cleartrip" src="https://${hostname}/mailers/mages/cleartrip_logo.png"/>

            </h2>

        </td>

    </tr>

    <tr>

        <td colspan="2" style="padding:0 30px 20px 25px;font-size:18px;color:#000000;">

            <p>We're very sorry, but we were unable to confirm your ${trip_name}  trip and you have been charged <#if amt_currency??> ${amt_currency}</#if> . Please do not panic &ndash; We are trying to confirm your booking. If the confirmation does not happen, we will refund the amount you have paid. Please call us in the next 15 minutes to know the latest on your booking. For any assistance please call: ${cleartrip_cust_support_no} (standard STD / local charges apply).</p>

    </td>

</tr>


</#if>

            </#if>

        </tr>

        <tr>

            <td style="padding:10px 10px 0px 25px;vertical-align:top;"><table width="100%" border="0" cellpadding="0" cellspacing="0"><tbody>

                <tr>

                    <td style="font-size:13px;color:#666666;font-weight:bold;letter-spacing:-0.2px;">Just booked</td>

                </tr>

                <#list  segments as segment >

                <tr>

                    <td>

                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-top:10px;padding-bottom:10px;">

                            <tr>

                                <td>

                                    <table style="padding-bottom:10px;" width="100%" border="0" cellpadding="0" cellspacing="0">

                                        <tbody><tr>

                                            <td width="37px;"><img src="https://www.cleartrip.com/images/logos/rail_logo.png" title="IRCTC" alt="IRCTC" style="font-size:10px;color:#999999;vertical-align:top;line-height:13px;" height="30px;" width="30px;"></td>

                                            <td style="font-size:12px;color:#444444;vertical-align:top;line-height:13px;" colspan="2">${segment.train_name} <br /> ${segment.train_number}</td>

                                        </tr>

                                        </tbody></table>

                                </td>

                            </tr>

                        </table>

                    </td>

                </tr>

                <tr>

                <td>

                    <table style="border-bottom:1px solid #eeeeee;padding-bottom:20px;" width="100%" border="0" cellpadding="0" cellspacing="0">

                    <tbody><tr>

                    <td style="font-size:13px;color:#000000;"><#if segment.departure_city_name??>${segment.departure_city_name}</#if>  &rarr; <#if segment.arrival_city_name??> ${segment.arrival_city_name} </#if> <br><div style="font-size:11px;color:#999999;margin-top:3px;"> ${segment.departure_date_time?datetime.iso?string("EEE, dd MM yyyy")}</div></td>

                <td style="font-size:13px;color:#000000; text-align: left;">${segment.departure_date_time?datetime.iso?string("HH:mm")}  &mdash; ${segment.arrival_date_time?datetime.iso?string("HH:mm")}  <br> <div style="font-size:11px;color:#999999;margin-top:3px;"> ${(segment.duration/(60*60))?floor}h, ${((segment.duration/(60*60)-((segment.duration/(60*60))?floor))*60)?round}m</div></td>

                <#-- td style="font-size:13px;color:#000000; text-align: left;">${segment.departure_date_time.strftime("%H:%M")  &mdash;  segment.arrival_date_time.strftime("%H:%M")  <br> <div style="font-size:11px;color:#999999;margin-top:3px;"> DateHelpers.pretty_print_duration(segment.duration).gsub("min", "m").split(", ").join(" ").strip}</div></td> -->

        </tr>

        </tbody></table>

        </td>

        </tr>

        </#list>


<tr>

<td style="font-size:13px;color:#${"666666"};font-weight:bold;padding-bottom:10px;padding-top:15px;">Travellers</td>

        </tr>

<tr>

<td>

    <table style="padding-bottom:10px;" width="100%" border="0" cellpadding="0" cellspacing="0">
    <#assign pax_type = {"ADT": "Adult", "CHD": "Child", "INF": "Infant", "SCM": "Senior Male","SCF": "Senior Female"} >
    <tbody>

    <#list  train_booking_infos as ti >

    <tr>

    <td style="font-size:12px; border-bottom:1px dotted #eeeeee; padding-bottom:10px; color:#000000; vertical-align:top; line-height:13px;">${ti.pax_info.title}  ${ti.pax_info.first_name} ${ti.pax_info.last_name}<br>

        <div style="font-size:11px;color:#999999;margin-top:3px;"><#if ti.pax_info.age?? > ${ti.pax_info.age} Yrs,  </#if> ${pax_type[ti.pax_info.pax_type_code]}</div>

    </td>

    <td style="font-size:10px; border-bottom:1px dotted #eeeeee; vertical-align:top;">
 <#assign train_waitlisted_status = ["WL","RLWL","CKWL","GNWL","PQWL","RQWL"] >
        <#if train_waitlisted_status?seq_contains(ti.confirmation_status) >

        <div style="background: none repeat scroll 0 0 #CC3333; color: #FFFFFF; border-radius: 3px; line-height: 1.6em; padding: 2px 4px; display:inline; font-weight: bold; text-transform: uppercase;">waitlist</div></td>

    <#elseif ti.confirmation_status == train_confirmation_status_rac >

    <div style="background: none repeat scroll 0 0 #ff9900; color: #FFFFFF; border-radius: 3px; line-height: 1.6em; padding: 2px 4px; display:inline; font-weight: bold; text-transform: uppercase;">RAC</div>

    <#else>

    <div style="background: none repeat scroll 0 0 #339900; color: #FFFFFF; border-radius: 3px; line-height: 1.6em; padding: 2px 4px; display:inline; font-weight: bold; text-transform: uppercase;">confirmed</div></td>

</#if>

<td style="font-size:11px; border-bottom:1px dotted #eeeeee; vertical-align:top;">${ti.seat_number_with_coach} </td>

        </tr>

        </#list>

        </tbody></table>

        </td>

        </tr>

        </tbody></table></td>
       <#assign payment_type = { "CC": "Credit Card", "DC": "Debit Card", "NB": "Net Banking", "DA": "Deposit Account", "KC": "Cash Card", "CA": "Cash", "TP": "Third Party", "TC": "Tech Process", "IV": "IVR", "WT": "Wallet Payment", "GV": "Gift Card", "TW": "Third Party Wallet", "NP": "No Payment", "UP": "UP Phonepe","AP": "Apple Pay"} >

            <#if payment_info["payments"]??>

            <td width="254px;" height="392px;" background="http://${hostname}/images/slip.png" style="vertical-align:top;background-image:url(http://${hostname}/images/slip.png);background-repeat:no-repeat;padding-right:30px;">

                <#else>

                <td style="vertical-align:top;padding-right:10px;width:272px" width="272px">

                </#if>

                <#if payment_info["payments"]??>

                <#-- include "src/main/resources/templates/_multiple_payment_receipt.ftl" -->
                <#else>

                <div style="height:18px;width:272px"><img src="https://${hostname}/images/mailers/sticky.gif" height="18" width="272" style="vertical-align:top"/></div><div style="padding:10px;border-right:1px solid #eeeeee;border-left:1px solid #eeeeee;">

                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:center;">

                        <tr>

                            <td style="font-size:10px;text-transform:uppercase;color:#666666;">Payment Receipt</td>

                        </tr>

                        <#if payment_type_key == "WT" || (payment_info["partial_wallet"]?? && payment_info["partial_wallet"])>
                <#if payment_info["partial_wallet"]??>

                <#if payment_info["other_payment_mode"].payment_type != "NB">

                <tr>

                <td>

                    <table width="100%" border="0" cellspacing="0" cellpadding="0">

                    <tbody>

                    <tr>

                    <td style="text-align:center;padding-right:5px;width:48%;padding-bottom:5px;">

                        <#if (payment_info["no_of_payments"][0]["logo_text"])?? >

                        <span style="font-size: 18px;font-weight: regular;color: #000000;text-align: center;">${payment_info["no_of_payments"][0]["logo_text"]}</span>

                        <#else>

                        <img src="https://${hostname}/images/mailers/ ${payment_info["no_of_payments"][0]["logo"]}_logo.png" style="padding:10px 0;font-size:18px;color:#000000;" alt=" ${payment_info["no_of_payments"][0]["payment_type"]}">

                    </#if>

                </td>

                <td style="text-align:left;color:#999999;padding-right:5px;width:2%;padding-bottom:5px;">

                    <span>+</span>

                </td>

                <td style="text-align:left;padding-left:5px;width:50%;padding-bottom:5px;">

                    <#if (payment_info["no_of_payments"][1]["logo_text"])?? >

                    <span style="font-size: 18px;font-weight: regular;color: #000000;text-align: center;">${payment_info["no_of_payments"][1]["logo_text"]}</span>

                    <#else>

                    <img src="https://${hostname}/images/mailers/${payment_info["no_of_payments"][1]["logo"]}_logo.png" style="padding:10px 0;font-size:18px;color:#000000;" alt="${payment_info["no_of_payments"][1]["payment_type"]}">

                </#if>

            </td>

        </tr>

        </tbody>

        </table>

        </td>

        </tr>

<tr>

<#elseif payment_info["other_payment_mode"].payment_type?? && payment_info["other_payment_mode"].payment_type == "NB">

<tr>

    <td>

        <img src="https://${hostname}/images/mailers/${payment_info['no_of_payments'][1]['logo']}_logo.png" style="padding:10px 0;font-size:18px;color:#000000;" alt="${payment_info['no_of_payments'][1]['payment_type']}">

    </td>

</tr>

<tr>

    <td style="border-top:1px solid #eeeeee;padding-bottom:10px;">

        <div style="color:#999999;font-size:10px;text-transform:uppercase;margin:10px 0 0px 0;">Netbanking through</div>

        <div style="color:#000000;font-size:18px;margin:0px;font-weight:bold;">${payment_info["no_of_payments"][0]["payment_type_id"]}</div>

    </td>

</tr>

</#if>

<#else>

<tr>

<#if payment_info["logo_text"]?? >

<span style="font-size: 18px;font-weight: regular;color: #000000;text-align: center;">${payment_info["logo_text"]}</span>

<#elseif payment_info["logo"] >

<td><img src="https://${hostname}/images/mailers/${payment_info['logo']}_logo.png" style="padding:10px 0;font-size:18px;color:#000000;" alt=" ${payment_info['payment_type']}">

</td>

<#else>

<td>

    <span style="font-size:18px;font-weight:bold;text-transform:uppercase;color:#000000;">${payment_info["payment_type"]}</style>

</td>

      </#if>

			</tr>

</#if>


                        <#else>

                        <tr>

                            <td style="padding-bottom:5px;">

                                <#if payment_info["logo_text"]?? >

                                <span style="font-size: 18px;font-weight: regular;color: #000000;text-align: center;">${payment_info["logo_text"]}</span>

                                <#elseif payment_info["logo"]?? >

                                <img src="https://${hostname}/images/mailers/${payment_info["logo"]}_logo.png" style="padding:10px 0;font-size:18px;color:#000000;" alt=<#if payment_info["payment_type"] ==  payment_type["TW"]>  ${payment_info["logo"]} <#else> ${payment_info["payment_type"]} </#if>>

                            <#else>

                            <span style="font-size:18px;font-weight:bold;text-transform:uppercase;color:#000000;"><#if payment_info["payment_type"]??>${payment_info["payment_type"]} </#if></style>

      </#if>

    </td>

  </tr>

  <tr>

    <td style="border-top:1px solid #eeeeee;padding-bottom:10px;">

      <div style="color:#999999;font-size:10px;text-transform:uppercase;margin:10px 0 0px 0;"><#if payment_info["payment_type"]??> ${payment_info["payment_type"]} </#if> </div>

      <div style="color:#000000;font-size:18px;margin:0px;font-weight:bold;"><#if payment_info["payment_type_id"]??>${payment_info["payment_type_id"]} </#if></div>

    </td>

  </tr>

 </#if>



  <tr>

    <td style="border-top:1px solid #eeeeee;padding-bottom:10px;">

      <div style="color:#999999;font-size:10px;text-transform:uppercase;margin:10px 0 0px 0;">Total charge</div>

      <div style="color:#000000;font-size:18px;margin:0px;font-weight:bold;">${format_total}</div>

    </td>

  </tr>

  <tr>

    <td style="border-top:1px solid #eeeeee;">

      <div style="color:#999999;font-size:10px;text-transform:uppercase;margin:10px 0 0px 0;"> Fare breakup</div>

       <#if has_free_cancellation>

       <div style="color:#000000;font-size:18px;margin:10px 0px;font-size:12px;">

      <#else>

       <div style="color:#000000;font-size:18px;margin:10px 0px;font-size:13px;">

      </#if>

        <table width="100%" border="0" cellspacing="0" cellpadding="0">

<#list payment_info["pricing_info"]  as   pricing_info>
          <tr>

            <td style="text-align:right;color:#999999;vertical-align:top;padding-right:5px;width:50%;padding-bottom:5px;">${pricing_info[0]}</td>

            <td style="text-align:left;color:#000000;vertical-align:top;padding-left:5px;width:50%;padding-bottom:5px;">${pricing_info[1]}</td> <#--GeneralHelpers::format_currency(pricing_info[1].abs, 0 ,currency)} -->

          </tr>

          </#list>

	   <#if additional_breakup??>

<#list additional_breakup as  add_bearkup>
	  <tr>

            <td style="text-align:right;color:#999999;vertical-align:top;padding-right:5px;width:50%;padding-bottom:5px;">${add_bearkup[0]}</td>

            <td style="text-align:left;color:#000000;vertical-align:top;padding-left:5px;width:50%;padding-bottom:5px;">${add_bearkup[1]}</td> <#--${GeneralHelpers::format_currency(add_bearkup[1].abs, 0 ,currency)} -->

          </tr>

	  </#list>

	  </#if>

          <tr>

            <td style="text-align:right;color:#999999;vertical-align:top;padding-right:5px;padding-bottom:5px;">Total</td>

            <td style="text-align:left;color:#000000;vertical-align:top;padding-left:5px;padding-bottom:5px;">${format_total}</td>

          </tr>

          <#if payment_info["hotel_part_pay"]?? && payment_info["hotel_part_pay"] == true >

          <tr>

            <td style="text-align:right;color:#999999;vertical-align:top;padding-right:5px;padding-bottom:5px;">Part payment</td>

            <td style="text-align:left;color:#000000;vertical-align:top;padding-left:5px;padding-bottom:5px;"> <#if part_payment??> ${part_payment} </#if></td>

          </tr>

          <tr>

            <td style="text-align:right;color:#999999;vertical-align:top;padding-right:5px;padding-bottom:5px;">Balance paid</td>

            <td style="text-align:left;color:#000000;vertical-align:top;padding-left:5px;padding-bottom:5px;">${balance_amount}</td>

          </tr>

          </#if>

        </table>

      </div>

    </td>

  </tr>

  </table>

</div>

<div style="height:18px;width:272px"><img src="https://${hostname}/images/mailers/perforation.gif" height="18" width="272" /></div>

</#if>


            </td>

        </tr>

<tr>

<td height="20px" colspan="2">&nbsp;</td>

</tr>



<#if !trip_corp_booking>

<tr>

<td colspan="2" style="background-color:#f4f4f4;">

    <img src="https://${hostname}/images/mailers/deal_ribbon.gif" width="575" height="30" alt="Exciting deal just for your" style=""/>

</td>

</tr>

<tr>

<td colspan="2" style="text-align:center;padding:20px 0 30px 0;background-color:#f4f4f4;" id="flightUpsell">

    <a href='${banner_link}' target='_blank' style="display:block;">

        <img src='${banner_image}' width='575' height='160' alt=''/>

    </a>

</td>

</tr>

<tr>

<td style="background-color:#f4f4f4;border-top:1px dashed #cccccc;" colspan="2" height="2px"></td>

</tr>

        </#if>

<#if (show_train_marketing_banner)?? >

        <tr>

            <td colspan="2" style="text-align:center;background-color:#f4f4f4;">

                <a href="https://www.surveymonkey.com/s/558CSQN"><img src="https://${hostname}/images/mailers/train_marketing_banner.jpg" alt="Redeem your cash back" height="60px;" width="428px;"></a>

            </td>

        </tr>

    </#if>

    <tr>

        <td style="background-color:#f4f4f4;padding:20px 25px 10px 25px;text-align:left;letter-spacing:-0.2px;" colspan="2">

            <#assign trip_type = 'train' >


            <table style="background-color:#f4f4f4;" width="100%" cellpadding="3" cellspacing="0">

                <#if p?? && p_confirmed_cleartrip_user >

                <#assign cells = 0 >

                <tr>

                    <#if cells < 2 && upsells?seq_contains("Cab") >

                    <#assign cabs_facility = false >


                    <#if cabs_facility >

                    <!-- Book a Cab -->

                    <td width="35px" style="vertical-align:top;"><img src="https://${hostname}/images/mailers/cab_icon.png" height="34px" width="34px" alt="Book a cab" style="font-size:11px;color:#999999;"></td>

                    <td width="220px" style="vertical-align:top;margin-right:10px;padding:0 15px 0 5px;font-size:15px; font-weight:bold; text-shadow:0 1px 0 #ffffff; color:#333333;">Book a cab <span style="font-size:11px;color:#999999;display:block;margin:2px 0 10px 0;font-weight:normal;">Book a cab to your departure airport. Service available in select cities.</span> <a href="https://${hostname}/account/trips/${trip_ref}?utm_source== ${trip_type}&utm_medium=email&utm_campaign=confirmation_email" style="font-size:13px;font-weight:normal;">Book a cab &rarr;</a></td>

                    <#assign cells = cells + 1>

                    <#else>

                    <!-- Book on Mobile-->

                    <td width="35px" style="vertical-align:top;"><img src="https://${hostname}/images/mailers/mobile_icon.png" height="34px" width="34px" alt="Book on the go" style="font-size:11px;color:#999999;"></td>

                    <td width="220px" style="vertical-align:top;margin-right:10px;padding:0 15px 0 5px;font-size:15px; font-weight:bold; text-shadow:0 1px 0 #ffffff; color:#333333;">Book on the go <span style="font-size:11px;color:#999999;display:block;margin:2px 0 10px 0;font-weight:normal;">Book flights, hotels && trains on the move with Cleartrip Mobile. </span> <a href="https://${hostname}/mobile?utm_source== ${trip_type}&utm_medium=email&utm_campaign=confirmation_email" style="font-size:13px;font-weight:normal;">Go mobile &rarr;</a></td>

                    <#assign cells = cells + 1>

                </#if>

            </#if>



            <#if cells < 2 && upsells?seq_contains("Expressway") >

            <#if !p_express_checkout_enabled >

            <!-- Express Way -->

            <td width="35px" style="vertical-align:top;"><img src="https://${hostname}/images/mailers/expressway_icon.png" height="34px" width="34px" alt="Expressway" style="font-size:11px;color:#999999;"></td>

            <td width="220px" style="vertical-align:top;margin-right:10px;padding:0 15px 0 5px;font-size:15px; font-weight:bold; text-shadow:0 1px 0 #ffffff; color:#333333;">Expressway <span style="font-size:11px;color:#999999;display:block;margin:2px 0 10px 0;font-weight:normal;">Save your card details once && book flights & hotels with 1 click thereafter.</span> <a href="https://${hostname}/expressway?utm_source==${trip_type}&utm_medium=email&utm_campaign=confirmation_email" style="font-size:13px;font-weight:normal;">Start by adding a card &rarr;</a></td>

            <#assign cells = cells + 1>

            <#else>

            <!-- Book Fare Alerts -->

            <td width="35px" style="vertical-align:top;"><img src="https://${hostname}/images/mailers/fare_alert_icon.png" height="34px" width="34px" alt="Hotel guarantee" style="font-size:11px;color:#999999;"></td>

            <td width="220px" style="vertical-align:top;margin-right:10px;padding:0 15px 0 5px;font-size:15px; font-weight:bold; text-shadow:0 1px 0 #ffffff; color:#333333;">Fare Alerts <span style="font-size:11px;color:#999999;display:block;margin:2px 0 10px 0;font-weight:normal;">Track fare history, get notifications in your inbox && save BIG. </span> <a href="https://${hostname}/alerts?utm_source==${trip_type}&utm_medium=email&utm_campaign=confirmation_email" style="font-size:13px;font-weight:normal;">Set Alert &rarr;</a></td>

            <#assign cells = cells + 1>

        </#if>

    </#if>



<#if cells < 2 && upsells?seq_contains("Mobile") >

        <!-- Book on Mobile-->

<td width="35px" style="vertical-align:top;"><img src="https://${hostname}/images/mailers/mobile_icon.png" height="34px" width="34px" alt="Book on the go" style="font-size:11px;color:#999999;"></td>

<td width="220px" style="vertical-align:top;margin-right:10px;padding:0 15px 0 5px;font-size:15px; font-weight:bold; text-shadow:0 1px 0 #ffffff; color:#333333;">Book on the go <span style="font-size:11px;color:#999999;display:block;margin:2px 0 10px 0;font-weight:normal;">Book flights, hotels and trains on the move with Cleartrip Mobile. </span> <a href="https://${hostname}/mobile?utm_source==${trip_type}&utm_medium=email&utm_campaign=confirmation_email" style="font-size:13px;font-weight:normal;">Go mobile &rarr;</a></td>

<#assign cells = cells + 1>

        </#if>



<#if cells < 2 && upsells?seq_contains("FareAlert") >

        <!-- Book Fare Alerts -->

<td width="35px" style="vertical-align:top;"><img src="https://${hostname}/images/mailers/fare_alert_icon.png" height="34px" width="34px" alt="Hotel guarantee" style="font-size:11px;color:#999999;"></td>

<td width="220px" style="vertical-align:top;margin-right:10px;padding:0 15px 0 5px;font-size:15px; font-weight:bold; text-shadow:0 1px 0 #ffffff; color:#333333;">Fare Alerts <span style="font-size:11px;color:#999999;display:block;margin:2px 0 10px 0;font-weight:normal;">Track fare history, get notifications in your inbox && save BIG. </span> <a href="https://${hostname}/alerts?utm_source=$[trip_type}&utm_medium=email&utm_campaign=confirmation_email" style="font-size:13px;font-weight:normal;">Set Alert &rarr;</a></td>

<#assign cells = cells + 1>

        </#if>



<#if cells < 2 && upsells?seq_contains("Quickeys") >

        <!-- Quickey -->

<td width="35px" style="vertical-align:top;"><img src="https://${hostname}/images/mailers/quickey_icon.png" height="34px" width="34px" alt="Expressway" style="font-size:11px;color:#999999;"></td>

<td width="220px" style="vertical-align:top;margin-right:10px;padding:0 15px 0 5px;font-size:15px; font-weight:bold; text-shadow:0 1px 0 #ffffff; color:#333333;">Quickeys <span style="font-size:11px;color:#999999;display:block;margin:2px 0 10px 0;font-weight:normal;">Get exclusive deals on hotel rooms for same day || next day check-in.</span> <a href="https://${hostname}/quickeys?utm_source==${trip_type}&utm_medium=email&utm_campaign=confirmation_email" style="font-size:13px;font-weight:normal;">Find Quickeys &rarr;</a></td>

<#assign cells = cells + 1>

        </#if>

        </tr>

<#else>




<#if !trip_corp_booking>

<tr>

<td colspan="8" style="padding:0px 0 20px 0;text-align:center;font-size:24px;color:#000;text-shadow:0 1px 1px #ffffff;font-weight:bold; ">

    It's time to get your own Cleartrip account

</td>

</tr>

<tr>

<td style="vertical-align:top" width="38px"><img src="https://${hostname}/images/mailers/print_icon.png" height="34px" width="34px" alt="Print tickets" style="font-size:10px;color:#999999;"></td>

<td style="vertical-align:top;font-size:12px;color:#666666;" width="80px">Print and <br/>email tickets</td>

<td style="vertical-align:top" width="38px"><img src="https://${hostname}/images/mailers/email_icon.png" height="34px" width="34px" alt="Email itinerary" style="font-size:10px;color:#999999;"></td>

<td style="vertical-align:top;font-size:12px;color:#666666;" width="80px">Email and <br/>SMS itinerary</td>

<td style="vertical-align:top" width="38px"><img src="https://${hostname}/images/mailers/account_icon.png" height="34px" width="34px" alt="Store travellers" style="font-size:10px;color:#999999;"></td>

<td style="vertical-align:top;font-size:12px;color:#666666;" width="80px">Store traveller information</td>

<td style="vertical-align:top" width="38px"><img src="https://${hostname}/images/mailers/cancellation_icon.png" height="34px" width="34px" alt="Online cancellations" style="font-size:10px;color:#999999;"></td>

<td style="vertical-align:top;font-size:12px;color:#666666;" width="80px">Easy online cancellations</td>

</tr>

<tr>

<td colspan="8" style="text-align:center;padding:20px 0 0 0;">

    <#if hide_button>

    <a href="${activation_url}" style="padding-top:5px;"><img src="https://${hostname}/images/mailers/get_account_button.png" alt="Get it now" border="0"></a>

</#if>

</td>

        </tr>

<#else>

<tr><td style="background-color:#f4f4f4;border-top:1px dashed #cccccc;" colspan="2" height="2px;"></td></tr>

<tr>

<td style="background-color:#f4f4f4;padding:20px 25px 10px 25px;text-align:left;letter-spacing:-0.2px;" colspan="2">

<table>

<tr>

    <td width="35px;" style="vertical-align:top;"><img src="https://${hostname}/images/account/account_icon.png" height="34px;" width="34px;" alt="Personal trip details" style="font-size:11px;color:#999999;"></td>

    <#if !hide_button>

    <#assign account_url = activation_url>

    <#else>

    <#assign account_url = "https://${hostname}/signin?trip_ref=${trip_ref}" >

</#if>

<td style="vertical-align:top;margin-right:10px;padding:0 15px 0 5px;font-size:15px; font-weight:bold; text-shadow:0 1px 0 #ffffff; color:#333333;">Send to personal account <span style="font-size:12px;color:#999999;display:block;margin:2px 0 10px 0;font-weight:normal;">Easily send this trip to your personal Cleartrip Account in a few steps. &nbsp;</span> <a href="${account_url}" style="font-size:13px;font-weight:normal;">Send trip to my account &rarr;</a></td>

<td width="35px;" style="vertical-align:top;"><img src="https://${hostname}/images/account/mobile_icon.png" height="34px;" width="34px;" alt="Cleartrip App" style="font-size:11px;color:#999999;"></td>

<td style="vertical-align:top;margin-right:10px;padding:0 10px 0 5px;font-size:15px; font-weight:bold; text-shadow:0 1px 0 #ffffff; color:#333333;">Download the Cleartrip App <span style="font-size:12px;color:#999999;display:block;margin:2px 0 10px 0;font-weight:normal;">Download the free Cleartrip App and carry the ticket with you where-ever you go.</span> <a href="https://${hostname}/m" style="font-size:13px;font-weight:normal;">Download the app &rarr;</a></td>

</tr>

        </table>

        </td>

        </tr>
        </#if>
        </#if>

        </table>


        </td>

    </tr>

</table>

</div>

<div style="height:40px;-webkit-border-radius:0 0 10px 10px;border-radius:0 0 10px 10px;background-color:#f4f4f4;"></div>

<p style="line-height:10px;padding:20px 0; margin:0;text-align:center; font-size:11px; color:#999;">&copy; 2006&ndash;${today_date} Cleartrip Private Limited</p>

</div>

</div>

        </body>

        </html>

