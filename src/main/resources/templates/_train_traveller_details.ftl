<tr>

  <td style="font-size:13px;color:#${"666666"};font-weight:bold;padding-bottom:10px;padding-top:15px;">Travellers</td>

</tr>

<tr>

  <td>

    <table style="padding-bottom:10px;" width="100%" border="0" cellpadding="0" cellspacing="0">

      <tbody>

        <#list  train_booking_infos as ti >

          <tr>

            <td style="font-size:12px; border-bottom:1px dotted #eeeeee; padding-bottom:10px; color:#000000; vertical-align:top; line-height:13px;">${ti.pax_info.title}  ${ti.pax_info.first_name} ${ti.pax_info.last_name}<br>

              <div style="font-size:11px;color:#999999;margin-top:3px;"><#if ti.pax_info.age?? > ${ti.pax_info.age} Yrs,  </#if> ${pax_type[ti.pax_info.pax_type_code]}</div>

            </td>

            <td style="font-size:10px; border-bottom:1px dotted #eeeeee; vertical-align:top;">

             <#if train_waitlisted_status?seq_contains(ti.confirmation_status) >

              <div style="background: none repeat scroll 0 0 #CC3333; color: #FFFFFF; border-radius: 3px; line-height: 1.6em; padding: 2px 4px; display:inline; font-weight: bold; text-transform: uppercase;">waitlist</div></td>

            <#elseif ti.confirmation_status == train_confirmation_status_rac >

              <div style="background: none repeat scroll 0 0 #ff9900; color: #FFFFFF; border-radius: 3px; line-height: 1.6em; padding: 2px 4px; display:inline; font-weight: bold; text-transform: uppercase;">RAC</div>

             <#else> 

              <div style="background: none repeat scroll 0 0 #339900; color: #FFFFFF; border-radius: 3px; line-height: 1.6em; padding: 2px 4px; display:inline; font-weight: bold; text-transform: uppercase;">confirmed</div></td>

            </#if> 

            <td style="font-size:11px; border-bottom:1px dotted #eeeeee; vertical-align:top;"><#if ti.seat_number_with_coach??>${ti.seat_number_with_coach}</#if></td>

          </tr>

       </#list> 

    </tbody></table>

  </td>

</tr>
