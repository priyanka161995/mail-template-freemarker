<div style="background:rgb(204,204,204);margin:0px;width:100%">
  <div style="margin:0px auto;padding:30px 0px 0px;width:575px"> 
    <p style="text-align:center"><img width="437" height="38" style="padding:10px 0px 0px;text-align:center;color:rgb(159,159,159);font-size:24px" alt="Thank you for booking with Cleartrip" src="https://ci5.googleusercontent.com/proxy/n_G9NzoTgkiobE1kEs85DhVQR5EeGOO-8i5F-TXHDD1PM3NCmfL6-wx0FITqG_lW5gw6QBIAGMQNxmCqmeaxiGf9lnSzqKKBl7Q0LNkSAQ=s0-d-e1-ft#https://www.cleartrip.com/images/mailers/thankyou_top.gif" class="CToWUd"></p>
    <div style="border-radius:10px 10px 0px 0px;height:10px;background-color:rgb(255,255,255)"></div>
    <div style="background-color:rgb(255,255,255)">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tbody><tr>
               </tr><tr>
  <td style="color:rgb(0,0,0);font-size:22px;font-weight:bold" bgcolor="#ffffff">
    <h1 style="color:rgb(0,0,0);padding-top:0px;padding-left:20px;font-size:24px">
        <img width="203" height="33" alt="Train confirmed" src="https://ci3.googleusercontent.com/proxy/9SsLArMSgtOMaueET_vtd9ExcuUuHfSuS69lFNdoCU91Et-lLVt3xwXOb3R9hkZUrQT3fD4H0kf6XOUHx_fw2beaU7jRGbOVTFteUJ8Bf7oOlPUedrcOBw=s0-d-e1-ft#https://www.cleartrip.com/images/mailers/train_confirmed_title.png" class="CToWUd">
    </h1>
  </td>
  <td style="text-align:right;padding-top:15px;padding-right:20px" bgcolor="#ffffff">
    <h2>
      <img width="137" height="45" alt="Cleartrip" src="https://ci4.googleusercontent.com/proxy/UsR1sDyXAeJfZdBIJfZa_4cK6yXsITj-LLuYBv0o-Kw82VqU-EnnDOQ4ZambwRLzuEk4GBIbvaIZ0CY6U6lxlZXOa2ggCeeTzTEqIBtNTyCn=s0-d-e1-ft#https://www.cleartrip.com/images/mailers/cleartrip_logo.png" class="CToWUd">
    </h2>
  </td>
</tr>
<tr>
  <td style="padding:0px 30px 20px 25px;color:rgb(0,0,0);font-size:18px" colspan="2">
      <p style="margin-bottom:10px">Hi ${username},</p>
      <p style="margin:10px 0px;line-height:23px">Your ${source} - ${destination} one-way train ticket is confirmed and attached with this email.</p>
      <p style="color:rgb(102,102,102);padding-top:10px;font-size:12px;margin-top:10px">Your Cleartrip Trip ID is <a href="http://email.tickets.cleartrip.com/c/eJxNj82KhTAMRp-m7q70z1YXLgaG-xoSY2oLtnVqff-pl1kMBJIcwuHLNk9q1KYL87DyAbSDyYJcF2H4IITYyOjRWqeY5uhTES_0JaeAB70KXT835UQv2-NRzx5z7COEo_OzFXYyVpICrd22OuRqtbQaDk7aacPumH2tJ1NfTL5bNR-UWsJH0nZAzHeqbXrg1boY-SCVsGqSnKn3XeNy5bsgMfVdC4TEpHlgpC3csUF6ovxBhHhC2FPDmJMLJUINOS2fm67MexN5qv0V0u6fV__H-QW8bV97" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://email.tickets.cleartrip.com/c/eJxNj82KhTAMRp-m7q70z1YXLgaG-xoSY2oLtnVqff-pl1kMBJIcwuHLNk9q1KYL87DyAbSDyYJcF2H4IITYyOjRWqeY5uhTES_0JaeAB70KXT835UQv2-NRzx5z7COEo_OzFXYyVpICrd22OuRqtbQaDk7aacPumH2tJ1NfTL5bNR-UWsJH0nZAzHeqbXrg1boY-SCVsGqSnKn3XeNy5bsgMfVdC4TEpHlgpC3csUF6ovxBhHhC2FPDmJMLJUINOS2fm67MexN5qv0V0u6fV__H-QW8bV97&amp;source=gmail&amp;ust=1527316281086000&amp;usg=AFQjCNGPVWrnULwf9LjcIw2j1ZAcrvPDqw"><strong>${trip_id}</strong></a>, you should use this in all communication with Cleartrip.</p>
        <p style="color:rgb(0,0,0);padding-top:10px;font-size:15px;margin-top:10px">Your PNR no. is <a title="Check your PNR status online" href="#m_-1754678121409764850_m_5663843163729851153_m_-5563702549473848886_m_-967371415755761118_"><strong>${pnr}</strong></a></p>
  </td>
</tr>

          
          <tr>
            <td style="padding:10px 10px 0px 25px;vertical-align:top"><table width="100%" border="0" cellspacing="0" cellpadding="0"><tbody>
              <tr>
  <td style="color:rgb(102,102,102);letter-spacing:-0.2px;font-size:13px;font-weight:bold">Just booked</td>
</tr>
<tr>
  <td>
    <table width="100%" style="padding-top:10px;padding-bottom:10px" border="0" cellspacing="0" cellpadding="0">
      <tbody><tr>
        <td>
          <table width="100%" style="padding-bottom:10px" border="0" cellspacing="0" cellpadding="0">
            <tbody><tr>
              <td width="37"><img width="30" height="30" title="IRCTC" style="color:rgb(153,153,153);line-height:13px;font-size:10px;vertical-align:top" alt="IRCTC" src="https://ci5.googleusercontent.com/proxy/VD1TGW4AWlWnk6_VttVsr8giw5PstENHMhEbjneAX3M9lxZkiop5SBE8i5lEP3ydFLOp9rZqAvNY7ewMUUkQkzGN6GaRNKAnsZU=s0-d-e1-ft#https://www.cleartrip.com/images/logos/rail_logo.png" class="CToWUd"></td>
              <td style="color:rgb(68,68,68);line-height:13px;font-size:12px;vertical-align:top" colspan="2">${train_name}<br>${train_number}</td>
            </tr>
          </tbody></table>
        </td>
      </tr>
    </tbody></table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%" style="padding-bottom:20px;border-bottom-color:rgb(238,238,238);border-bottom-width:1px;border-bottom-style:solid" border="0" cellspacing="0" cellpadding="0">
      <tbody><tr>
        <td style="color:rgb(0,0,0);font-size:13px">${source} → ${destination}<br><div style="color:rgb(153,153,153);font-size:11px;margin-top:3px"><span class="aBn" data-term="goog_1851831923" tabindex="0"><span class="aQJ">${train_start_date}</span></span></div></td>
        <td style="text-align:left;color:rgb(0,0,0);font-size:13px"><span class="aBn" data-term="goog_1851831924" tabindex="0"><span class="aQJ">${dep_time} — ${arr_time}</span></span> <br> <div style="color:rgb(153,153,153);font-size:11px;margin-top:3px">${duration}</div></td>
      </tr>
    </tbody></table>
  </td>
</tr>

              <tr>
  <td style="color:rgb(102,102,102);padding-top:15px;padding-bottom:10px;font-size:13px;font-weight:bold">Travellers</td>
</tr>
<tr>
  <td>
    <table width="100%" style="padding-bottom:10px" border="0" cellspacing="0" cellpadding="0">
      <tbody>
        <#list travellers as travel>
          <tr>
            <td style="color:rgb(0,0,0);line-height:13px;padding-bottom:10px;font-size:12px;vertical-align:top;border-bottom-color:rgb(238,238,238);border-bottom-width:1px;border-bottom-style:dotted">${travel.name}<br>
              <div style="color:rgb(153,153,153);font-size:11px;margin-top:3px">${travel.info}</div>
            </td>
            <td style="font-size:10px;vertical-align:top;border-bottom-color:rgb(238,238,238);border-bottom-width:1px;border-bottom-style:dotted">
              <div style="background:0px 0px rgb(51,153,0);padding:2px 4px;border-radius:3px;color:rgb(255,255,255);text-transform:uppercase;line-height:1.6em;font-weight:bold;display:inline">${travel.status}</div></td>
            <td style="font-size:11px;vertical-align:top;border-bottom-color:rgb(238,238,238);border-bottom-width:1px;border-bottom-style:dotted">${travel.seat_no}</td>
          </tr>
          </#list>
    </tbody></table>
  </td>
</tr>

            </tbody></table></td>
              <td width="272" style="width:272px;padding-right:10px;vertical-align:top">
              <div style="width:272px;height:18px"><img width="272" height="18" style="vertical-align:top" src="https://ci5.googleusercontent.com/proxy/OZYbnHbbnw0v_GqUpY5FOHI8qwYjWqzMmdnSmjnJq1eKGtMPl7RlF0958ONrG52RwVf1FzjANwltNtD5rG7KRGApK1zHZ1PgQA=s0-d-e1-ft#https://www.cleartrip.com/images/mailers/sticky.gif" class="CToWUd"></div><div style="padding:10px;border-right-color:rgb(238,238,238);border-left-color:rgb(238,238,238);border-right-width:1px;border-left-width:1px;border-right-style:solid;border-left-style:solid">
<table width="100%" style="text-align:center" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td style="color:rgb(102,102,102);text-transform:uppercase;font-size:10px">Payment Receipt</td>
  </tr>
  <#if payment_mode == "Debit Card">
  <tr>
    <td style="padding-bottom:5px">
      <img style="padding:10px 0px;color:rgb(0,0,0);font-size:18px" alt="Debit Card" src="https://ci5.googleusercontent.com/proxy/WmeJTeAaDn3CslUhE6SJ806l-8UdT4pNaSe2rr4xoB4HFzzk7XkIMKeclGJ90grvwlsrJMRg3H92GOCLrZepN1ktNh78R9nijayYV5Yn50s1yw=s0-d-e1-ft#https://www.cleartrip.com/images/mailers/mastercard_logo.png" class="CToWUd">
    </td>
  </tr>
  <tr>
    <td style="padding-bottom:10px;border-top-color:rgb(238,238,238);border-top-width:1px;border-top-style:solid">
      <div style="margin:10px 0px 0px;color:rgb(153,153,153);text-transform:uppercase;font-size:10px">Debit Card</div>
      <div style="margin:0px;color:rgb(0,0,0);font-size:18px;font-weight:bold">${card_number}</div>
    </td>
  </tr>
  </#if>
  <tr>
    <td style="padding-bottom:10px;border-top-color:rgb(238,238,238);border-top-width:1px;border-top-style:solid">
      <div style="margin:10px 0px 0px;color:rgb(153,153,153);text-transform:uppercase;font-size:10px">Total charge</div>
      <div style="margin:0px;color:rgb(0,0,0);font-size:18px;font-weight:bold">${total_fare}</div>
    </td>
  </tr>
  <tr>
    <td style="border-top-color:rgb(238,238,238);border-top-width:1px;border-top-style:solid">
      <div style="margin:10px 0px 0px;color:rgb(153,153,153);text-transform:uppercase;font-size:10px">Fare breakup</div>
       <div style="margin:10px 0px;color:rgb(0,0,0);font-size:13px">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tbody><tr>
            <td style="width:50%;text-align:right;color:rgb(153,153,153);padding-right:5px;padding-bottom:5px;vertical-align:top">Base Fare</td>
            <td style="width:50%;text-align:left;color:rgb(0,0,0);padding-bottom:5px;padding-left:5px;vertical-align:top">${basic_fare}</td>
          </tr>
          <tr>
            <td style="width:50%;text-align:right;color:rgb(153,153,153);padding-right:5px;padding-bottom:5px;vertical-align:top">Railway Charges</td>
            <td style="width:50%;text-align:left;color:rgb(0,0,0);padding-bottom:5px;padding-left:5px;vertical-align:top">${railway_charges}</td>
          </tr>
          <tr>
            <td style="width:50%;text-align:right;color:rgb(153,153,153);padding-right:5px;padding-bottom:5px;vertical-align:top">Cleartrip Service Fee</td>
            <td style="width:50%;text-align:left;color:rgb(0,0,0);padding-bottom:5px;padding-left:5px;vertical-align:top">${ct_service_fee}</td>
          </tr>
          <tr>
            <td style="width:50%;text-align:right;color:rgb(153,153,153);padding-right:5px;padding-bottom:5px;vertical-align:top">Processing Fee</td>
            <td style="width:50%;text-align:left;color:rgb(0,0,0);padding-bottom:5px;padding-left:5px;vertical-align:top">${processing_fee}</td>
          </tr>
          <tr>
            <td style="text-align:right;color:rgb(153,153,153);padding-right:5px;padding-bottom:5px;vertical-align:top">Total</td>
            <td style="text-align:left;color:rgb(0,0,0);padding-bottom:5px;padding-left:5px;vertical-align:top">${total_fare}</td>
          </tr>
        </tbody></table>
      </div>
    </td>
  </tr>
  </tbody></table>
</div>
<div style="width:272px;height:18px"><img width="272" height="18" src="https://ci4.googleusercontent.com/proxy/u9SR3KJW-P3wIt0QoRlU0S1cxN9ZkmlwzzgZvjlCR69hCxLq6ZLdQoN8wAfJIAcqbfJDklJPhJaTawgvfRp-uHgQ6Xu-X3b3mQYBdlyn=s0-d-e1-ft#https://www.cleartrip.com/images/mailers/perforation.gif" class="CToWUd"></div>

            </td>
          </tr>
          <tr>
  <td height="20" colspan="2">&nbsp;</td>
</tr>
<tr>
  <td style="background-color:rgb(244,244,244)" colspan="2">
    <img width="575" height="30" alt="Exciting deal just for your" src="https://ci5.googleusercontent.com/proxy/ZNdy2BskdOC4UeVYeXOf9CjUU4y2isk-9QgCEgnUVVLuTgzfH3lYwFoXbkrLLshtQSlpLxR4AYmuYECE4mohMOZ_eduo63CB6N5JRhc9=s0-d-e1-ft#https://www.cleartrip.com/images/mailers/deal_ribbon.gif" class="CToWUd">
  </td>
</tr>
<tr>
  <td id="m_-1754678121409764850m_5663843163729851153m_-5563702549473848886m_-967371415755761118flightUpsell" style="padding:20px 0px 30px;text-align:center;background-color:rgb(244,244,244)" colspan="2">
    <a style="display:block" href="http://email.tickets.cleartrip.com/c/eJxNjktuwzAMRE8j7yzoZyleaFGgyDUKhWQioYrt0kqM9PRVuyrAxcMA84YYZ3tyfihxuqgpuWuaQzKXD-3VpLVG8u4UwtUKpyAvrEfIvC4FKo1M-9eD1oXGIKG2TcJ6l_dU6pAjatcLQMYED1ZjmLUich4QaTI-DTXm1rZd2Ddhzv2O4-gSSty4_Jl6Rgl34idxZ6RaOr06wqfc8ibs-btvFxT2fVYDx9uDIVOTe1lu-ffd_7YfXG5KvA" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://email.tickets.cleartrip.com/c/eJxNjktuwzAMRE8j7yzoZyleaFGgyDUKhWQioYrt0kqM9PRVuyrAxcMA84YYZ3tyfihxuqgpuWuaQzKXD-3VpLVG8u4UwtUKpyAvrEfIvC4FKo1M-9eD1oXGIKG2TcJ6l_dU6pAjatcLQMYED1ZjmLUich4QaTI-DTXm1rZd2Ddhzv2O4-gSSty4_Jl6Rgl34idxZ6RaOr06wqfc8ibs-btvFxT2fVYDx9uDIVOTe1lu-ffd_7YfXG5KvA&amp;source=gmail&amp;ust=1527316281086000&amp;usg=AFQjCNElC_u7MSTbF6GySGndfvvB0oUdcA">
    	<img width="575" height="160" alt="" src="https://ci5.googleusercontent.com/proxy/fKsSGd6qpBiQ-kx0B1rEPP3oq9J8qRZJ1k9CehpT63eJOS5vR4VWrym8HdvQAYOdoeczJU6nx2rSj4o5NpK6r28OHqL-9OyJGCnc0fD2hKsztJIchhTsogE2pgKT1va3i9Q=s0-d-e1-ft#https://www.cleartrip.com/eadserver/delivery/avw.php?zoneid=90+&amp;amp;cb=88964" class="CToWUd">
    </a>
  </td>
</tr>
<tr>
  <td height="2" style="border-top-color:rgb(204,204,204);border-top-width:1px;border-top-style:dashed;background-color:rgb(244,244,244)" colspan="2"></td>
</tr>

          <tr>
            <td style="padding:20px 25px 10px;text-align:left;letter-spacing:-0.2px;background-color:rgb(244,244,244)" colspan="2">
              <table width="100%" style="background-color:rgb(244,244,244)" cellspacing="0" cellpadding="3">
  <tbody><tr>
      
      <td width="35" style="vertical-align:top"><img width="34" height="34" style="color:rgb(153,153,153);font-size:11px" alt="Book on the go" src="https://ci5.googleusercontent.com/proxy/K29j71ZPEv8FHui9aVk88e0b-eSIDVxo7Ov6vSYlSf2hI1ekdkZM6CM7lv5pYo5Dj2ParKoFsXdZlFYMunjz_Rw71LFDhmxFFDIC5til=s0-d-e1-ft#https://www.cleartrip.com/images/mailers/mobile_icon.png" class="CToWUd"></td>
      <td width="220" style="padding:0px 15px 0px 5px;color:rgb(51,51,51);font-size:15px;font-weight:bold;margin-right:10px;vertical-align:top">Book on the go <span style="margin:2px 0px 10px;color:rgb(153,153,153);font-size:11px;font-weight:normal;display:block">Book flights, hotels and trains on the move with Cleartrip Mobile. </span> <a style="font-size:13px;font-weight:normal" href="http://email.tickets.cleartrip.com/c/eJxNj0GOhCAQRU-DOw0ooi5cTDLpaxgsCqlEwEGM1x_s9KKT2tTLr_dTZp66UaqK5n7lvZZWT4Nu10Uo3gshDCo5DoPtmOTgQhI1uBQDwY51wvPvwhiwHhrY89FA9I3XtFduHldbDpQyk-AWRw4cRlSmLf5BQm-rfXY5Hyfrflj7KnPfd5GgTjnR21SYjyvtyLrXlf1yxitBWX5z0hRYqx7o0dDlC8Sn9wNB-0PTFgqGGCwlrzPFsLwzVZq3InKYm5PC5p6_vmv_AQG0Wzg" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://email.tickets.cleartrip.com/c/eJxNj0GOhCAQRU-DOw0ooi5cTDLpaxgsCqlEwEGM1x_s9KKT2tTLr_dTZp66UaqK5n7lvZZWT4Nu10Uo3gshDCo5DoPtmOTgQhI1uBQDwY51wvPvwhiwHhrY89FA9I3XtFduHldbDpQyk-AWRw4cRlSmLf5BQm-rfXY5Hyfrflj7KnPfd5GgTjnR21SYjyvtyLrXlf1yxitBWX5z0hRYqx7o0dDlC8Sn9wNB-0PTFgqGGCwlrzPFsLwzVZq3InKYm5PC5p6_vmv_AQG0Wzg&amp;source=gmail&amp;ust=1527316281086000&amp;usg=AFQjCNET7141iui_mBpDuqTm6bL5HbNNkw">Go mobile →</a></td>

      
      <td width="35" style="vertical-align:top"><img width="34" height="34" style="color:rgb(153,153,153);font-size:11px" alt="Expressway" src="https://ci4.googleusercontent.com/proxy/rh8qCrLGF339YA0DXbRKlVMnJ3_NGHmdpaGsj1YxdKuP_Ml8OaGBtcDx0OawoscBFVIL_mS38t0fBqrY01YaStjWXxouMRU5t43ut0YpFNLSjw=s0-d-e1-ft#https://www.cleartrip.com/images/mailers/expressway_icon.png" class="CToWUd"></td>
      <td width="220" style="padding:0px 15px 0px 5px;color:rgb(51,51,51);font-size:15px;font-weight:bold;margin-right:10px;vertical-align:top">Expressway <span style="margin:2px 0px 10px;color:rgb(153,153,153);font-size:11px;font-weight:normal;display:block">Save your card details once and book flights &amp; hotels with 1 click thereafter.</span> <a style="font-size:13px;font-weight:normal" href="http://email.tickets.cleartrip.com/c/eJxNj0FuhSAQQE-DOw0Igi5cNGn-NcwIg5AIWMTY3r7400WTWb3MvJcx88RHIRs_DysdQFiYFPTrwiQdGGMGpRiVspwIql3MrNUup-j1jm3G8-vCFLFVnd7L0ekUugB-b9ysrZiM5AMwNtR7GA3tkdrJqBXB0rXZZ1fKcRL-QfpXnfu-qwQhl-zfpsrw-6iN84Yfwl9XCcuZrqyR8M-SwUfSywcGNP4KFeLT_oMawgF-ixXrFK3PAYpPcXnvNHneqshh6U4fN_f89j_9C5vyXRg" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://email.tickets.cleartrip.com/c/eJxNj0FuhSAQQE-DOw0Igi5cNGn-NcwIg5AIWMTY3r7400WTWb3MvJcx88RHIRs_DysdQFiYFPTrwiQdGGMGpRiVspwIql3MrNUup-j1jm3G8-vCFLFVnd7L0ekUugB-b9ysrZiM5AMwNtR7GA3tkdrJqBXB0rXZZ1fKcRL-QfpXnfu-qwQhl-zfpsrw-6iN84Yfwl9XCcuZrqyR8M-SwUfSywcGNP4KFeLT_oMawgF-ixXrFK3PAYpPcXnvNHneqshh6U4fN_f89j_9C5vyXRg&amp;source=gmail&amp;ust=1527316281086000&amp;usg=AFQjCNG1DNxKsYN7Ea03oTrYz5kAPZ_TgA">Start by adding a card →</a></td>



  </tr>
</tbody></table>

            </td>
          </tr>
        </tbody></table>
      </div>
      <div style="border-radius:0px 0px 10px 10px;height:40px;background-color:rgb(244,244,244)"></div>
<p style="margin:0px;padding:20px 0px;text-align:center;color:rgb(153,153,153);line-height:10px;font-size:11px">© 2006–2018 Cleartrip Private Limited</p>
    </div>
  </div>