package com.memorynotfound.mail.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.HashMap;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MyPojo
{
    private String format_total;

    private String total;

    private String amendment;

    private Con con;

    private boolean train_booking_info_exists;

    private String trip_booking_status;

    private boolean trip_fully_confirmed;

    private boolean trip_convert_failure_to_success_activated;

    private boolean trip_unconfirmed_status;

    private String banner_image;

    private String train_confirmation_status_rac;

    private String activation_url;

    private String[] train_waitlisted_status;

    private boolean p_express_checkout_enabled;

    private String banner_image_prefix;

    private String trip_name;

    private String confirmation_subject;

    private String relative_file_path;

    private boolean trip_b2c_booking;

    private String hostname;

    private Trip_train_booking trip_train_booking;

    private String cleartrip_cust_support_no;

    private boolean google_card_enabled;

    private String mime_partial;

    private boolean hide_button;

    private String show_train_marketing_banner;

    private Train_booking_infos[] train_booking_infos;

    private HashMap<String,String> pax_type;

    private boolean has_free_cancellation;

    private Payment_info payment_info;

    private Segments[] segments;

    private String subject;

    private Object hashBannerData;

    private String payment_type_key;

    private String from;

    private boolean trip_has_failed_parts;

    private String currency;

    private boolean ticket_attached;

    private String banner_link;

    private String trip_ref;

    private String logo;

    private boolean trip_train_booking_exists;

    private String[] recipients;

    private String pnr_number;

    private String[] upsells;

    private String amt_currency;

    private String firstname;

    private String country_code;

    private boolean trip_corp_booking;

    private String banner_file_name;

    private Object p;

    private String from_address;

    private boolean p_confirmed_cleartrip_user;

    private String today_date;

    private String user;

    private HashMap<String,String> payment_type;

    private String trip_details_page;

    public String getFormat_total ()
    {
        return format_total;
    }

    public void setFormat_total (String format_total)
    {
        this.format_total = format_total;
    }

    public String getTotal ()
    {
        return total;
    }

    public void setTotal (String total)
    {
        this.total = total;
    }

    public String getAmendment ()
    {
        return amendment;
    }

    public void setAmendment (String amendment)
    {
        this.amendment = amendment;
    }

    public Con getCon ()
    {
        return con;
    }

    public void setCon (Con con)
    {
        this.con = con;
    }

    public boolean getTrain_booking_info_exists ()
    {
        return train_booking_info_exists;
    }

    public void setTrain_booking_info_exists (boolean train_booking_info_exists)
    {
        this.train_booking_info_exists = train_booking_info_exists;
    }

    public String getTrip_booking_status ()
    {
        return trip_booking_status;
    }

    public void setTrip_booking_status (String trip_booking_status)
    {
        this.trip_booking_status = trip_booking_status;
    }

    public boolean getTrip_fully_confirmed ()
    {
        return trip_fully_confirmed;
    }

    public void setTrip_fully_confirmed (boolean trip_fully_confirmed)
    {
        this.trip_fully_confirmed = trip_fully_confirmed;
    }

    public boolean getTrip_convert_failure_to_success_activated ()
    {
        return trip_convert_failure_to_success_activated;
    }

    public void setTrip_convert_failure_to_success_activated (boolean trip_convert_failure_to_success_activated)
    {
        this.trip_convert_failure_to_success_activated = trip_convert_failure_to_success_activated;
    }

    public boolean getTrip_unconfirmed_status ()
    {
        return trip_unconfirmed_status;
    }

    public void setTrip_unconfirmed_status (boolean trip_unconfirmed_status)
    {
        this.trip_unconfirmed_status = trip_unconfirmed_status;
    }

    public String getBanner_image ()
    {
        return banner_image;
    }

    public void setBanner_image (String banner_image)
    {
        this.banner_image = banner_image;
    }

    public String getTrain_confirmation_status_rac ()
    {
        return train_confirmation_status_rac;
    }

    public void setTrain_confirmation_status_rac (String train_confirmation_status_rac)
    {
        this.train_confirmation_status_rac = train_confirmation_status_rac;
    }

    public String getActivation_url ()
    {
        return activation_url;
    }

    public void setActivation_url (String activation_url)
    {
        this.activation_url = activation_url;
    }

    public String[] getTrain_waitlisted_status ()
    {
        return train_waitlisted_status;
    }

    public void setTrain_waitlisted_status (String[] train_waitlisted_status)
    {
        this.train_waitlisted_status = train_waitlisted_status;
    }

    public boolean getP_express_checkout_enabled ()
{
    return p_express_checkout_enabled;
}

    public void setP_express_checkout_enabled (boolean p_express_checkout_enabled)
    {
        this.p_express_checkout_enabled = p_express_checkout_enabled;
    }

    public String getBanner_image_prefix ()
    {
        return banner_image_prefix;
    }

    public void setBanner_image_prefix (String banner_image_prefix)
    {
        this.banner_image_prefix = banner_image_prefix;
    }

    public String getTrip_name ()
    {
        return trip_name;
    }

    public void setTrip_name (String trip_name)
    {
        this.trip_name = trip_name;
    }

    public String getConfirmation_subject ()
    {
        return confirmation_subject;
    }

    public void setConfirmation_subject (String confirmation_subject)
    {
        this.confirmation_subject = confirmation_subject;
    }

    public String getRelative_file_path ()
    {
        return relative_file_path;
    }

    public void setRelative_file_path (String relative_file_path)
    {
        this.relative_file_path = relative_file_path;
    }

    public boolean getTrip_b2c_booking ()
    {
        return trip_b2c_booking;
    }

    public void setTrip_b2c_booking (boolean trip_b2c_booking)
    {
        this.trip_b2c_booking = trip_b2c_booking;
    }

    public String getHostname ()
    {
        return hostname;
    }

    public void setHostname (String hostname)
    {
        this.hostname = hostname;
    }

    public Trip_train_booking getTrip_train_booking ()
    {
        return trip_train_booking;
    }

    public void setTrip_train_booking (Trip_train_booking trip_train_booking)
    {
        this.trip_train_booking = trip_train_booking;
    }

    public String getCleartrip_cust_support_no ()
    {
        return cleartrip_cust_support_no;
    }

    public void setCleartrip_cust_support_no (String cleartrip_cust_support_no)
    {
        this.cleartrip_cust_support_no = cleartrip_cust_support_no;
    }

    public boolean getGoogle_card_enabled ()
    {
        return google_card_enabled;
    }

    public void setGoogle_card_enabled (boolean google_card_enabled)
    {
        this.google_card_enabled = google_card_enabled;
    }

    public String getMime_partial ()
    {
        return mime_partial;
    }

    public void setMime_partial (String mime_partial)
    {
        this.mime_partial = mime_partial;
    }

    public boolean getHide_button ()
    {
        return hide_button;
    }

    public void setHide_button (boolean hide_button)
    {
        this.hide_button = hide_button;
    }

    public String getShow_train_marketing_banner ()
    {
        return show_train_marketing_banner;
    }

    public void setShow_train_marketing_banner (String show_train_marketing_banner)
    {
        this.show_train_marketing_banner = show_train_marketing_banner;
    }

    public Train_booking_infos[] getTrain_booking_infos ()
    {
        return train_booking_infos;
    }

    public void setTrain_booking_infos (Train_booking_infos[] train_booking_infos)
    {
        this.train_booking_infos = train_booking_infos;
    }

    public HashMap<String, String> getPax_type ()
    {
        return pax_type;
    }

    public void setPax_type (HashMap<String,String> pax_type)
    {
        this.pax_type = pax_type;
    }

    public boolean getHas_free_cancellation ()
    {
        return has_free_cancellation;
    }

    public void setHas_free_cancellation (boolean has_free_cancellation)
    {
        this.has_free_cancellation = has_free_cancellation;
    }

    public Payment_info getPayment_info ()
    {
        return payment_info;
    }

    public void setPayment_info (Payment_info payment_info)
    {
        this.payment_info = payment_info;
    }

    public Segments[] getSegments ()
    {
        return segments;
    }

    public void setSegments (Segments[] segments)
    {
        this.segments = segments;
    }

    public String getSubject ()
    {
        return subject;
    }

    public void setSubject (String subject)
    {
        this.subject = subject;
    }

    public Object getHashBannerData ()
    {
        return hashBannerData;
    }

    public void setHashBannerData (Object hashBannerData)
    {
        this.hashBannerData = hashBannerData;
    }

    public String getPayment_type_key ()
    {
        return payment_type_key;
    }

    public void setPayment_type_key (String payment_type_key)
    {
        this.payment_type_key = payment_type_key;
    }

    public String getFrom ()
    {
        return from;
    }

    public void setFrom (String from)
    {
        this.from = from;
    }

    public boolean getTrip_has_failed_parts ()
    {
        return trip_has_failed_parts;
    }

    public void setTrip_has_failed_parts (boolean trip_has_failed_parts)
    {
        this.trip_has_failed_parts = trip_has_failed_parts;
    }

    public String getCurrency ()
    {
        return currency;
    }

    public void setCurrency (String currency)
    {
        this.currency = currency;
    }

    public boolean getTicket_attached ()
    {
        return ticket_attached;
    }

    public void setTicket_attached (boolean ticket_attached)
    {
        this.ticket_attached = ticket_attached;
    }

    public String getBanner_link ()
    {
        return banner_link;
    }

    public void setBanner_link (String banner_link)
    {
        this.banner_link = banner_link;
    }

    public String getTrip_ref ()
    {
        return trip_ref;
    }

    public void setTrip_ref (String trip_ref)
    {
        this.trip_ref = trip_ref;
    }

    public String getLogo ()
    {
        return logo;
    }

    public void setLogo (String logo)
    {
        this.logo = logo;
    }

    public boolean getTrip_train_booking_exists ()
    {
        return trip_train_booking_exists;
    }

    public void setTrip_train_booking_exists (boolean trip_train_booking_exists)
    {
        this.trip_train_booking_exists = trip_train_booking_exists;
    }

    public String[] getRecipients ()
    {
        return recipients;
    }

    public void setRecipients (String[] recipients)
    {
        this.recipients = recipients;
    }

    public String getPnr_number ()
    {
        return pnr_number;
    }

    public void setPnr_number (String pnr_number)
    {
        this.pnr_number = pnr_number;
    }

    public String[] getUpsells ()
    {
        return upsells;
    }

    public void setUpsells (String[] upsells)
    {
        this.upsells = upsells;
    }

    public String getAmt_currency ()
    {
        return amt_currency;
    }

    public void setAmt_currency (String amt_currency)
    {
        this.amt_currency = amt_currency;
    }

    public String getFirstname ()
    {
        return firstname;
    }

    public void setFirstname (String firstname)
    {
        this.firstname = firstname;
    }

    public String getCountry_code ()
    {
        return country_code;
    }

    public void setCountry_code (String country_code)
    {
        this.country_code = country_code;
    }

    public boolean getTrip_corp_booking ()
    {
        return trip_corp_booking;
    }

    public void setTrip_corp_booking (boolean trip_corp_booking)
    {
        this.trip_corp_booking = trip_corp_booking;
    }

    public String getBanner_file_name ()
    {
        return banner_file_name;
    }

    public void setBanner_file_name (String banner_file_name)
    {
        this.banner_file_name = banner_file_name;
    }

    public Object getP ()
    {
        return p;
    }

    public void setP (Object p)
    {
        this.p = p;
    }

    public String getFrom_address ()
    {
        return from_address;
    }

    public void setFrom_address (String from_address)
    {
        this.from_address = from_address;
    }

    public boolean getP_confirmed_cleartrip_user ()
    {
        return p_confirmed_cleartrip_user;
    }

    public void setP_confirmed_cleartrip_user (boolean p_confirmed_cleartrip_user)
    {
        this.p_confirmed_cleartrip_user = p_confirmed_cleartrip_user;
    }

    public String getToday_date ()
    {
        return today_date;
    }

    public void setToday_date (String today_date)
    {
        this.today_date = today_date;
    }

    public String getUser ()
    {
        return user;
    }

    public void setUser (String user)
    {
        this.user = user;
    }

    public HashMap<String,String> getPayment_type ()
    {
        return payment_type;
    }

    public void setPayment_type (HashMap<String,String> payment_type)
    {
        this.payment_type = payment_type;
    }

    public String getTrip_details_page ()
{
    return trip_details_page;
}

    public void setTrip_details_page (String trip_details_page)
    {
        this.trip_details_page = trip_details_page;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [format_total = "+format_total+", total = "+total+", amendment = "+amendment+", con = "+con+", train_booking_info_exists = "+train_booking_info_exists+", trip_booking_status = "+trip_booking_status+", trip_fully_confirmed = "+trip_fully_confirmed+", trip_convert_failure_to_success_activated = "+trip_convert_failure_to_success_activated+", trip_unconfirmed_status = "+trip_unconfirmed_status+", banner_image = "+banner_image+", train_confirmation_status_rac = "+train_confirmation_status_rac+", activation_url = "+activation_url+", train_waitlisted_status = "+train_waitlisted_status+", p_express_checkout_enabled = "+p_express_checkout_enabled+", banner_image_prefix = "+banner_image_prefix+", trip_name = "+trip_name+", confirmation_subject = "+confirmation_subject+", relative_file_path = "+relative_file_path+", trip_b2c_booking = "+trip_b2c_booking+", hostname = "+hostname+", trip_train_booking = "+trip_train_booking+", cleartrip_cust_support_no = "+cleartrip_cust_support_no+", google_card_enabled = "+google_card_enabled+", mime_partial = "+mime_partial+", hide_button = "+hide_button+", show_train_marketing_banner = "+show_train_marketing_banner+", train_booking_infos = "+train_booking_infos+", pax_type = "+pax_type+", has_free_cancellation = "+has_free_cancellation+", payment_info = "+payment_info+", segments = "+segments+", subject = "+subject+", hashBannerData = "+hashBannerData+", payment_type_key = "+payment_type_key+", from = "+from+", trip_has_failed_parts = "+trip_has_failed_parts+", currency = "+currency+", ticket_attached = "+ticket_attached+", banner_link = "+banner_link+", trip_ref = "+trip_ref+", logo = "+logo+", trip_train_booking_exists = "+trip_train_booking_exists+", recipients = "+recipients+", pnr_number = "+pnr_number+", upsells = "+upsells+", amt_currency = "+amt_currency+", firstname = "+firstname+", country_code = "+country_code+", trip_corp_booking = "+trip_corp_booking+", banner_file_name = "+banner_file_name+", p = "+p+", from_address = "+from_address+", p_confirmed_cleartrip_user = "+p_confirmed_cleartrip_user+", today_date = "+today_date+", user = "+user+", payment_type = "+payment_type+", trip_details_page = "+trip_details_page+"]";
    }
}
			
			