package com.memorynotfound.mail.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Pax_info
{
    private String birth_country;

    private String linkable_id;

    private String person_id;

    private String middle_name;

    private String meal_request_code;

    private String date_of_birth;

    private String pax_nationality;

    private String linkable_type;

    private String redress_number;

    private String total_fare;

    private String pax_type_code;

    private String trip_id;

    private String id;

    private String first_name;

    private String title;

    private String updated_at;

    private String seq_no;

    private String age;

    private String last_name;

    private String created_at;

    private String gender;

    private String air_booking_id;

    private Object external_refs;

    private Object freq_flier_numbers;

    public String getBirth_country ()
{
    return birth_country;
}

    public void setBirth_country (String birth_country)
    {
        this.birth_country = birth_country;
    }

    public String getLinkable_id ()
    {
        return linkable_id;
    }

    public void setLinkable_id (String linkable_id)
    {
        this.linkable_id = linkable_id;
    }

    public String getPerson_id ()
    {
        return person_id;
    }

    public void setPerson_id (String person_id)
    {
        this.person_id = person_id;
    }

    public String getMiddle_name ()
{
    return middle_name;
}

    public void setMiddle_name (String middle_name)
    {
        this.middle_name = middle_name;
    }

    public String getMeal_request_code ()
{
    return meal_request_code;
}

    public void setMeal_request_code (String meal_request_code)
    {
        this.meal_request_code = meal_request_code;
    }

    public String getDate_of_birth ()
{
    return date_of_birth;
}

    public void setDate_of_birth (String date_of_birth)
    {
        this.date_of_birth = date_of_birth;
    }

    public String getPax_nationality ()
{
    return pax_nationality;
}

    public void setPax_nationality (String pax_nationality)
    {
        this.pax_nationality = pax_nationality;
    }

    public String getLinkable_type ()
    {
        return linkable_type;
    }

    public void setLinkable_type (String linkable_type)
    {
        this.linkable_type = linkable_type;
    }

    public String getRedress_number ()
{
    return redress_number;
}

    public void setRedress_number (String redress_number)
    {
        this.redress_number = redress_number;
    }

    public String getTotal_fare ()
    {
        return total_fare;
    }

    public void setTotal_fare (String total_fare)
    {
        this.total_fare = total_fare;
    }

    public String getPax_type_code ()
    {
        return pax_type_code;
    }

    public void setPax_type_code (String pax_type_code)
    {
        this.pax_type_code = pax_type_code;
    }

    public String getTrip_id ()
    {
        return trip_id;
    }

    public void setTrip_id (String trip_id)
    {
        this.trip_id = trip_id;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getFirst_name ()
    {
        return first_name;
    }

    public void setFirst_name (String first_name)
    {
        this.first_name = first_name;
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public String getUpdated_at ()
    {
        return updated_at;
    }

    public void setUpdated_at (String updated_at)
    {
        this.updated_at = updated_at;
    }

    public String getSeq_no ()
    {
        return seq_no;
    }

    public void setSeq_no (String seq_no)
    {
        this.seq_no = seq_no;
    }

    public String getAge ()
    {
        return age;
    }

    public void setAge (String age)
    {
        this.age = age;
    }

    public String getLast_name ()
    {
        return last_name;
    }

    public void setLast_name (String last_name)
    {
        this.last_name = last_name;
    }

    public String getCreated_at ()
    {
        return created_at;
    }

    public void setCreated_at (String created_at)
    {
        this.created_at = created_at;
    }

    public String getGender ()
    {
        return gender;
    }

    public void setGender (String gender)
    {
        this.gender = gender;
    }

    public String getAir_booking_id ()
    {
        return air_booking_id;
    }

    public void setAir_booking_id (String air_booking_id)
    {
        this.air_booking_id = air_booking_id;
    }

    public Object getExternal_refs ()
{
    return external_refs;
}

    public void setExternal_refs (Object external_refs)
    {
        this.external_refs = external_refs;
    }

    public Object getFreq_flier_numbers ()
{
    return freq_flier_numbers;
}

    public void setFreq_flier_numbers (Object freq_flier_numbers)
    {
        this.freq_flier_numbers = freq_flier_numbers;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [birth_country = "+birth_country+", linkable_id = "+linkable_id+", person_id = "+person_id+", middle_name = "+middle_name+", meal_request_code = "+meal_request_code+", date_of_birth = "+date_of_birth+", pax_nationality = "+pax_nationality+", linkable_type = "+linkable_type+", redress_number = "+redress_number+", total_fare = "+total_fare+", pax_type_code = "+pax_type_code+", trip_id = "+trip_id+", id = "+id+", first_name = "+first_name+", title = "+title+", updated_at = "+updated_at+", seq_no = "+seq_no+", age = "+age+", last_name = "+last_name+", created_at = "+created_at+", gender = "+gender+", air_booking_id = "+air_booking_id+", external_refs = "+external_refs+", freq_flier_numbers = "+freq_flier_numbers+"]";
    }
}

