package com.memorynotfound.mail.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Trip_train_booking
{
    private String trip_id;

    private String id;

    private String updated_at;

    private String seq_no;

    private String booking_status;

    private String is_refundable;

    private String created_at;

    private String total_fare;

    private String train_key;

    public String getTrip_id ()
    {
        return trip_id;
    }

    public void setTrip_id (String trip_id)
    {
        this.trip_id = trip_id;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getUpdated_at ()
    {
        return updated_at;
    }

    public void setUpdated_at (String updated_at)
    {
        this.updated_at = updated_at;
    }

    public String getSeq_no ()
    {
        return seq_no;
    }

    public void setSeq_no (String seq_no)
    {
        this.seq_no = seq_no;
    }

    public String getBooking_status ()
    {
        return booking_status;
    }

    public void setBooking_status (String booking_status)
    {
        this.booking_status = booking_status;
    }

    public String getIs_refundable ()
    {
        return is_refundable;
    }

    public void setIs_refundable (String is_refundable)
    {
        this.is_refundable = is_refundable;
    }

    public String getCreated_at ()
    {
        return created_at;
    }

    public void setCreated_at (String created_at)
    {
        this.created_at = created_at;
    }

    public String getTotal_fare ()
    {
        return total_fare;
    }

    public void setTotal_fare (String total_fare)
    {
        this.total_fare = total_fare;
    }

    public String getTrain_key ()
{
    return train_key;
}

    public void setTrain_key (String train_key)
    {
        this.train_key = train_key;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [trip_id = "+trip_id+", id = "+id+", updated_at = "+updated_at+", seq_no = "+seq_no+", booking_status = "+booking_status+", is_refundable = "+is_refundable+", created_at = "+created_at+", total_fare = "+total_fare+", train_key = "+train_key+"]";
    }
}

