package com.memorynotfound.mail.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Segments
{
    private String boarding_date_time;
    private String arrival_city_name;
    private String departure_city_name;

    public String getArrival_city_name() {
        return arrival_city_name;
    }

    public void setArrival_city_name(String arrival_city_name) {
        this.arrival_city_name = arrival_city_name;
    }

    public String getDeparture_city_name() {
        return departure_city_name;
    }

    public void setDeparture_city_name(String departure_city_name) {
        this.departure_city_name = departure_city_name;
    }

    private String departure_date_time;

    private String booking_status;

    private String train_number;

    private String master_pax_info_id;

    private String train_route_id;

    private String master_pax_info_seq_no;

    private float arrival_halt_duration;

    private String id;

    private String train_name;

    private float duration;

    private String distance;

    private String updated_at;

    private String departure_halt_duration;

    private String seq_no;

    private String departure_station;

    private String boarding_station;

    private String arrival_station;

    private String destination_station;

    private String arrival_date_time;

    private String created_at;

    private String source_station;

    private String train_type;

    private String booking_quota;

    public String getBoarding_date_time ()
    {
        return boarding_date_time;
    }

    public void setBoarding_date_time (String boarding_date_time)
    {
        this.boarding_date_time = boarding_date_time;
    }

    public String getDeparture_date_time ()
    {
        return departure_date_time;
    }

    public void setDeparture_date_time (String departure_date_time)
    {
        this.departure_date_time = departure_date_time;
    }

    public String getBooking_status ()
    {
        return booking_status;
    }

    public void setBooking_status (String booking_status)
    {
        this.booking_status = booking_status;
    }

    public String getTrain_number ()
    {
        return train_number;
    }

    public void setTrain_number (String train_number)
    {
        this.train_number = train_number;
    }

    public String getMaster_pax_info_id ()
{
    return master_pax_info_id;
}

    public void setMaster_pax_info_id (String master_pax_info_id)
    {
        this.master_pax_info_id = master_pax_info_id;
    }

    public String getTrain_route_id ()
    {
        return train_route_id;
    }

    public void setTrain_route_id (String train_route_id)
    {
        this.train_route_id = train_route_id;
    }

    public String getMaster_pax_info_seq_no ()
{
    return master_pax_info_seq_no;
}

    public void setMaster_pax_info_seq_no (String master_pax_info_seq_no)
    {
        this.master_pax_info_seq_no = master_pax_info_seq_no;
    }

    public float getArrival_halt_duration ()
{
    return arrival_halt_duration;
}

    public void setArrival_halt_duration (float arrival_halt_duration)
    {
        this.arrival_halt_duration = arrival_halt_duration;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getTrain_name ()
    {
        return train_name;
    }

    public void setTrain_name (String train_name)
    {
        this.train_name = train_name;
    }

    public float getDuration ()
    {
        return duration;
    }

    public void setDuration (float duration)
    {
        this.duration = duration;
    }

    public String getDistance ()
    {
        return distance;
    }

    public void setDistance (String distance)
    {
        this.distance = distance;
    }

    public String getUpdated_at ()
    {
        return updated_at;
    }

    public void setUpdated_at (String updated_at)
    {
        this.updated_at = updated_at;
    }

    public String getDeparture_halt_duration ()
{
    return departure_halt_duration;
}

    public void setDeparture_halt_duration (String departure_halt_duration)
    {
        this.departure_halt_duration = departure_halt_duration;
    }

    public String getSeq_no ()
    {
        return seq_no;
    }

    public void setSeq_no (String seq_no)
    {
        this.seq_no = seq_no;
    }

    public String getDeparture_station ()
    {
        return departure_station;
    }

    public void setDeparture_station (String departure_station)
    {
        this.departure_station = departure_station;
    }

    public String getBoarding_station ()
    {
        return boarding_station;
    }

    public void setBoarding_station (String boarding_station)
    {
        this.boarding_station = boarding_station;
    }

    public String getArrival_station ()
    {
        return arrival_station;
    }

    public void setArrival_station (String arrival_station)
    {
        this.arrival_station = arrival_station;
    }

    public String getDestination_station ()
{
    return destination_station;
}

    public void setDestination_station (String destination_station)
    {
        this.destination_station = destination_station;
    }

    public String getArrival_date_time ()
    {
        return arrival_date_time;
    }

    public void setArrival_date_time (String arrival_date_time)
    {
        this.arrival_date_time = arrival_date_time;
    }

    public String getCreated_at ()
    {
        return created_at;
    }

    public void setCreated_at (String created_at)
    {
        this.created_at = created_at;
    }

    public String getSource_station ()
{
    return source_station;
}

    public void setSource_station (String source_station)
    {
        this.source_station = source_station;
    }

    public String getTrain_type ()
{
    return train_type;
}

    public void setTrain_type (String train_type)
    {
        this.train_type = train_type;
    }

    public String getBooking_quota ()
    {
        return booking_quota;
    }

    public void setBooking_quota (String booking_quota)
    {
        this.booking_quota = booking_quota;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [boarding_date_time = "+boarding_date_time+", departure_date_time = "+departure_date_time+", booking_status = "+booking_status+", train_number = "+train_number+", master_pax_info_id = "+master_pax_info_id+", train_route_id = "+train_route_id+", master_pax_info_seq_no = "+master_pax_info_seq_no+", arrival_halt_duration = "+arrival_halt_duration+", id = "+id+", train_name = "+train_name+", duration = "+duration+", distance = "+distance+", updated_at = "+updated_at+", departure_halt_duration = "+departure_halt_duration+", seq_no = "+seq_no+", departure_station = "+departure_station+", boarding_station = "+boarding_station+", arrival_station = "+arrival_station+", destination_station = "+destination_station+", arrival_date_time = "+arrival_date_time+", created_at = "+created_at+", source_station = "+source_station+", train_type = "+train_type+", booking_quota = "+booking_quota+"]";
    }
}

