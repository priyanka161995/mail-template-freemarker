package com.memorynotfound.mail.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Train_booking_infos
{
    private String pax_info_seq_no;

    private String slip_route_msg;

    private Pax_info pax_info;

    private String wait_list_number;

    private String ticket_number;

    private String initial_confirmation_status;

    private String id;

    private String seq_no;

    private String created_at;

    private String train_fare_id;

    private String train_fare_seq_no;

    private String train_seq_no;

    private String status_reason;

    private String coach_number;

    private String booking_status;

    private String pnr_number;

    private String confirmation_status;

    private String train_id;

    private String berth_preference;

    private String berth_type;

    private String train_booking_id;

    private String booking_class;

    private String updated_at;

    private String pax_info_id;

    private String seat_number;

    private String seat_number_with_coach;

    public String getSeat_number_with_coach() {
        return seat_number_with_coach;
    }

    public void setSeat_number_with_coach(String seat_number_with_coach) {
        this.seat_number_with_coach = seat_number_with_coach;
    }

    public String getPax_info_seq_no ()
    {
        return pax_info_seq_no;
    }

    public void setPax_info_seq_no (String pax_info_seq_no)
    {
        this.pax_info_seq_no = pax_info_seq_no;
    }

    public String getSlip_route_msg ()
{
    return slip_route_msg;
}

    public void setSlip_route_msg (String slip_route_msg)
    {
        this.slip_route_msg = slip_route_msg;
    }

    public Pax_info getPax_info ()
    {
        return pax_info;
    }

    public void setPax_info (Pax_info pax_info)
    {
        this.pax_info = pax_info;
    }

    public String getWait_list_number ()
    {
        return wait_list_number;
    }

    public void setWait_list_number (String wait_list_number)
    {
        this.wait_list_number = wait_list_number;
    }

    public String getTicket_number ()
    {
        return ticket_number;
    }

    public void setTicket_number (String ticket_number)
    {
        this.ticket_number = ticket_number;
    }

    public String getInitial_confirmation_status ()
    {
        return initial_confirmation_status;
    }

    public void setInitial_confirmation_status (String initial_confirmation_status)
    {
        this.initial_confirmation_status = initial_confirmation_status;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getSeq_no ()
    {
        return seq_no;
    }

    public void setSeq_no (String seq_no)
    {
        this.seq_no = seq_no;
    }

    public String getCreated_at ()
    {
        return created_at;
    }

    public void setCreated_at (String created_at)
    {
        this.created_at = created_at;
    }

    public String getTrain_fare_id ()
    {
        return train_fare_id;
    }

    public void setTrain_fare_id (String train_fare_id)
    {
        this.train_fare_id = train_fare_id;
    }

    public String getTrain_fare_seq_no ()
    {
        return train_fare_seq_no;
    }

    public void setTrain_fare_seq_no (String train_fare_seq_no)
    {
        this.train_fare_seq_no = train_fare_seq_no;
    }

    public String getTrain_seq_no ()
    {
        return train_seq_no;
    }

    public void setTrain_seq_no (String train_seq_no)
    {
        this.train_seq_no = train_seq_no;
    }

    public String getStatus_reason ()
{
    return status_reason;
}

    public void setStatus_reason (String status_reason)
    {
        this.status_reason = status_reason;
    }

    public String getCoach_number ()
{
    return coach_number;
}

    public void setCoach_number (String coach_number)
    {
        this.coach_number = coach_number;
    }

    public String getBooking_status ()
    {
        return booking_status;
    }

    public void setBooking_status (String booking_status)
    {
        this.booking_status = booking_status;
    }

    public String getPnr_number ()
    {
        return pnr_number;
    }

    public void setPnr_number (String pnr_number)
    {
        this.pnr_number = pnr_number;
    }

    public String getConfirmation_status ()
    {
        return confirmation_status;
    }

    public void setConfirmation_status (String confirmation_status)
    {
        this.confirmation_status = confirmation_status;
    }

    public String getTrain_id ()
    {
        return train_id;
    }

    public void setTrain_id (String train_id)
    {
        this.train_id = train_id;
    }

    public String getBerth_preference ()
    {
        return berth_preference;
    }

    public void setBerth_preference (String berth_preference)
    {
        this.berth_preference = berth_preference;
    }

    public String getBerth_type ()
{
    return berth_type;
}

    public void setBerth_type (String berth_type)
    {
        this.berth_type = berth_type;
    }

    public String getTrain_booking_id ()
    {
        return train_booking_id;
    }

    public void setTrain_booking_id (String train_booking_id)
    {
        this.train_booking_id = train_booking_id;
    }

    public String getBooking_class ()
    {
        return booking_class;
    }

    public void setBooking_class (String booking_class)
    {
        this.booking_class = booking_class;
    }

    public String getUpdated_at ()
    {
        return updated_at;
    }

    public void setUpdated_at (String updated_at)
    {
        this.updated_at = updated_at;
    }

    public String getPax_info_id ()
    {
        return pax_info_id;
    }

    public void setPax_info_id (String pax_info_id)
    {
        this.pax_info_id = pax_info_id;
    }

    public String getSeat_number ()
    {
        return seat_number;
    }

    public void setSeat_number (String seat_number)
    {
        this.seat_number = seat_number;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [pax_info_seq_no = "+pax_info_seq_no+", slip_route_msg = "+slip_route_msg+", pax_info = "+pax_info+", wait_list_number = "+wait_list_number+", ticket_number = "+ticket_number+", initial_confirmation_status = "+initial_confirmation_status+", id = "+id+", seq_no = "+seq_no+", created_at = "+created_at+", train_fare_id = "+train_fare_id+", train_fare_seq_no = "+train_fare_seq_no+", train_seq_no = "+train_seq_no+", status_reason = "+status_reason+", coach_number = "+coach_number+", booking_status = "+booking_status+", pnr_number = "+pnr_number+", confirmation_status = "+confirmation_status+", train_id = "+train_id+", berth_preference = "+berth_preference+", berth_type = "+berth_type+", train_booking_id = "+train_booking_id+", booking_class = "+booking_class+", updated_at = "+updated_at+", pax_info_id = "+pax_info_id+", seat_number = "+seat_number+"]";
    }
}

