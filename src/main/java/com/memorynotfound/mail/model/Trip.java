package com.memorynotfound.mail.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Trip
{
    private String[] insurances;

    private String affiliate_txn_id;

    private String itinerary_id;

    private String user_key;

    private String user_trip_name;

    private String[] hotel_bookings;

    private String amount;

    private String train;

    private String email_id;

    private String trip_name;

    private boolean has_wallet_promotion;

    private Contact_detail contact_detail;

    private String booking_status;

    private String latest_tkt_date;

    private Travellers travellers;

    private String customer_no;

    private String bkg_type;

    private String workflow;

    private String end_date_time;

    private String updated_at;

    private String trip_type;

    private Payments[] payments;

    private String air;

    private String company_id;

    private String contact_detail_id;

    private String[] activity_bookings;

    private String route_happy_score;

    private String start_date_time;

    private String currency;

    private String id;

    private String booked_user_id;

    private String domain;

    private String created_at;

    private String user_id;

    private String site_language;

    private String activity;

    private Object tags;

    private String trip_ref;

    private String cur_inr_value;

    private String raterule_coupon_id;

    private String[] air_bookings;

    private boolean customer_confirmation_eligible;

    private Object pta_emergency_book;

    private Object misc;

    private String package_savings;

    private String express_checkout;

    private String txn_source_type;

    private String trip_key;

    private String hotel;

    public String[] getInsurances ()
    {
        return insurances;
    }

    public void setInsurances (String[] insurances)
    {
        this.insurances = insurances;
    }

    public String getAffiliate_txn_id ()
{
    return affiliate_txn_id;
}

    public void setAffiliate_txn_id (String affiliate_txn_id)
    {
        this.affiliate_txn_id = affiliate_txn_id;
    }

    public String getItinerary_id ()
    {
        return itinerary_id;
    }

    public void setItinerary_id (String itinerary_id)
    {
        this.itinerary_id = itinerary_id;
    }

    public String getUser_key ()
{
    return user_key;
}

    public void setUser_key (String user_key)
    {
        this.user_key = user_key;
    }

    public String getUser_trip_name ()
    {
        return user_trip_name;
    }

    public void setUser_trip_name (String user_trip_name)
    {
        this.user_trip_name = user_trip_name;
    }

    public String[] getHotel_bookings ()
    {
        return hotel_bookings;
    }

    public void setHotel_bookings (String[] hotel_bookings)
    {
        this.hotel_bookings = hotel_bookings;
    }

    public String getAmount ()
    {
        return amount;
    }

    public void setAmount (String amount)
    {
        this.amount = amount;
    }

    public String getTrain ()
    {
        return train;
    }

    public void setTrain (String train)
    {
        this.train = train;
    }

    public String getEmail_id ()
    {
        return email_id;
    }

    public void setEmail_id (String email_id)
    {
        this.email_id = email_id;
    }

    public String getTrip_name ()
    {
        return trip_name;
    }

    public void setTrip_name (String trip_name)
    {
        this.trip_name = trip_name;
    }

    public boolean getHas_wallet_promotion ()
{
    return has_wallet_promotion;
}

    public void setHas_wallet_promotion (boolean has_wallet_promotion)
    {
        this.has_wallet_promotion = has_wallet_promotion;
    }

    public Contact_detail getContact_detail ()
    {
        return contact_detail;
    }

    public void setContact_detail (Contact_detail contact_detail)
    {
        this.contact_detail = contact_detail;
    }

    public String getBooking_status ()
    {
        return booking_status;
    }

    public void setBooking_status (String booking_status)
    {
        this.booking_status = booking_status;
    }

    public String getLatest_tkt_date ()
{
    return latest_tkt_date;
}

    public void setLatest_tkt_date (String latest_tkt_date)
    {
        this.latest_tkt_date = latest_tkt_date;
    }

    public Travellers getTravellers ()
    {
        return travellers;
    }

    public void setTravellers (Travellers travellers)
    {
        this.travellers = travellers;
    }

    public String getCustomer_no ()
{
    return customer_no;
}

    public void setCustomer_no (String customer_no)
    {
        this.customer_no = customer_no;
    }

    public String getBkg_type ()
{
    return bkg_type;
}

    public void setBkg_type (String bkg_type)
    {
        this.bkg_type = bkg_type;
    }

    public String getWorkflow ()
{
    return workflow;
}

    public void setWorkflow (String workflow)
    {
        this.workflow = workflow;
    }

    public String getEnd_date_time ()
    {
        return end_date_time;
    }

    public void setEnd_date_time (String end_date_time)
    {
        this.end_date_time = end_date_time;
    }

    public String getUpdated_at ()
    {
        return updated_at;
    }

    public void setUpdated_at (String updated_at)
    {
        this.updated_at = updated_at;
    }

    public String getTrip_type ()
    {
        return trip_type;
    }

    public void setTrip_type (String trip_type)
    {
        this.trip_type = trip_type;
    }

    public Payments[] getPayments ()
    {
        return payments;
    }

    public void setPayments (Payments[] payments)
    {
        this.payments = payments;
    }

    public String getAir ()
    {
        return air;
    }

    public void setAir (String air)
    {
        this.air = air;
    }

    public String getCompany_id ()
    {
        return company_id;
    }

    public void setCompany_id (String company_id)
    {
        this.company_id = company_id;
    }

    public String getContact_detail_id ()
{
    return contact_detail_id;
}

    public void setContact_detail_id (String contact_detail_id)
    {
        this.contact_detail_id = contact_detail_id;
    }

    public String[] getActivity_bookings ()
    {
        return activity_bookings;
    }

    public void setActivity_bookings (String[] activity_bookings)
    {
        this.activity_bookings = activity_bookings;
    }

    public String getRoute_happy_score ()
{
    return route_happy_score;
}

    public void setRoute_happy_score (String route_happy_score)
    {
        this.route_happy_score = route_happy_score;
    }

    public String getStart_date_time ()
    {
        return start_date_time;
    }

    public void setStart_date_time (String start_date_time)
    {
        this.start_date_time = start_date_time;
    }

    public String getCurrency ()
    {
        return currency;
    }

    public void setCurrency (String currency)
    {
        this.currency = currency;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getBooked_user_id ()
    {
        return booked_user_id;
    }

    public void setBooked_user_id (String booked_user_id)
    {
        this.booked_user_id = booked_user_id;
    }

    public String getDomain ()
    {
        return domain;
    }

    public void setDomain (String domain)
    {
        this.domain = domain;
    }

    public String getCreated_at ()
    {
        return created_at;
    }

    public void setCreated_at (String created_at)
    {
        this.created_at = created_at;
    }

    public String getUser_id ()
    {
        return user_id;
    }

    public void setUser_id (String user_id)
    {
        this.user_id = user_id;
    }


    public String getSite_language ()
{
    return site_language;
}

    public void setSite_language (String site_language)
    {
        this.site_language = site_language;
    }

    public String getActivity ()
    {
        return activity;
    }

    public void setActivity (String activity)
    {
        this.activity = activity;
    }

    public Object getTags ()
{
    return tags;
}

    public void setTags (Object tags)
    {
        this.tags = tags;
    }

    public String getTrip_ref ()
    {
        return trip_ref;
    }

    public void setTrip_ref (String trip_ref)
    {
        this.trip_ref = trip_ref;
    }

    public String getCur_inr_value ()
{
    return cur_inr_value;
}

    public void setCur_inr_value (String cur_inr_value)
    {
        this.cur_inr_value = cur_inr_value;
    }

    public String getRaterule_coupon_id ()
{
    return raterule_coupon_id;
}

    public void setRaterule_coupon_id (String raterule_coupon_id)
    {
        this.raterule_coupon_id = raterule_coupon_id;
    }

    public String[] getAir_bookings ()
    {
        return air_bookings;
    }

    public void setAir_bookings (String[] air_bookings)
    {
        this.air_bookings = air_bookings;
    }

    public boolean getCustomer_confirmation_eligible ()
{
    return customer_confirmation_eligible;
}

    public void setCustomer_confirmation_eligible (boolean customer_confirmation_eligible)
    {
        this.customer_confirmation_eligible = customer_confirmation_eligible;
    }

    public Object getPta_emergency_book ()
{
    return pta_emergency_book;
}

    public void setPta_emergency_book (Object pta_emergency_book)
    {
        this.pta_emergency_book = pta_emergency_book;
    }

    public Object getMisc ()
{
    return misc;
}

    public void setMisc (Object misc)
    {
        this.misc = misc;
    }

    public String getPackage_savings ()
{
    return package_savings;
}

    public void setPackage_savings (String package_savings)
    {
        this.package_savings = package_savings;
    }

    public String getExpress_checkout ()
{
    return express_checkout;
}

    public void setExpress_checkout (String express_checkout)
    {
        this.express_checkout = express_checkout;
    }

    public String getTxn_source_type ()
    {
        return txn_source_type;
    }

    public void setTxn_source_type (String txn_source_type)
    {
        this.txn_source_type = txn_source_type;
    }

    public String getTrip_key ()
{
    return trip_key;
}

    public void setTrip_key (String trip_key)
    {
        this.trip_key = trip_key;
    }

    public String getHotel ()
    {
        return hotel;
    }

    public void setHotel (String hotel)
    {
        this.hotel = hotel;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [insurances = "+insurances+", affiliate_txn_id = "+affiliate_txn_id+", itinerary_id = "+itinerary_id+", user_key = "+user_key+", user_trip_name = "+user_trip_name+", hotel_bookings = "+hotel_bookings+", amount = "+amount+", train = "+train+", email_id = "+email_id+", trip_name = "+trip_name+", has_wallet_promotion = "+has_wallet_promotion+", contact_detail = "+contact_detail+", booking_status = "+booking_status+", latest_tkt_date = "+latest_tkt_date+", travellers = "+travellers+", customer_no = "+customer_no+", bkg_type = "+bkg_type+", workflow = "+workflow+", end_date_time = "+end_date_time+", updated_at = "+updated_at+", trip_type = "+trip_type+", payments = "+payments+", air = "+air+", company_id = "+company_id+", contact_detail_id = "+contact_detail_id+", activity_bookings = "+activity_bookings+", route_happy_score = "+route_happy_score+", start_date_time = "+start_date_time+", currency = "+currency+", id = "+id+", booked_user_id = "+booked_user_id+", domain = "+domain+", created_at = "+created_at+", user_id = "+user_id+", site_language = "+site_language+", activity = "+activity+", tags = "+tags+", trip_ref = "+trip_ref+", cur_inr_value = "+cur_inr_value+", raterule_coupon_id = "+raterule_coupon_id+", air_bookings = "+air_bookings+", customer_confirmation_eligible = "+customer_confirmation_eligible+", pta_emergency_book = "+pta_emergency_book+", misc = "+misc+", package_savings = "+package_savings+", express_checkout = "+express_checkout+", txn_source_type = "+txn_source_type+", trip_key = "+trip_key+", hotel = "+hotel+"]";
    }
}

