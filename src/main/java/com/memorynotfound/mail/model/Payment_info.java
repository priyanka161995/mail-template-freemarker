package com.memorynotfound.mail.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Payment_info
{
    private String logo;

    private String payment_subtype;

    private String[][] pricing_info;

    private String payment_type_name;

    private String logo_text;

    private String payment_type;

    private String payment_type_id;

    public String getLogo ()
    {
        return logo;
    }

    public void setLogo (String logo)
    {
        this.logo = logo;
    }

    public String getPayment_subtype ()
{
    return payment_subtype;
}

    public void setPayment_subtype (String payment_subtype)
    {
        this.payment_subtype = payment_subtype;
    }

    public String[][] getPricing_info ()
    {
        return pricing_info;
    }

    public void setPricing_info (String[][] pricing_info)
    {
        this.pricing_info = pricing_info;
    }

    public String getPayment_type_name ()
    {
        return payment_type_name;
    }

    public void setPayment_type_name (String payment_type_name)
    {
        this.payment_type_name = payment_type_name;
    }

    public String getLogo_text ()
{
    return logo_text;
}

    public void setLogo_text (String logo_text)
    {
        this.logo_text = logo_text;
    }

    public String getPayment_type ()
    {
        return payment_type;
    }

    public void setPayment_type (String payment_type)
    {
        this.payment_type = payment_type;
    }

    public String getPayment_type_id ()
    {
        return payment_type_id;
    }

    public void setPayment_type_id (String payment_type_id)
    {
        this.payment_type_id = payment_type_id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [logo = "+logo+", payment_subtype = "+payment_subtype+", pricing_info = "+pricing_info+", payment_type_name = "+payment_type_name+", logo_text = "+logo_text+", payment_type = "+payment_type+", payment_type_id = "+payment_type_id+"]";
    }
}

