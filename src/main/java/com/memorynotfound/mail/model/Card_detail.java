package com.memorynotfound.mail.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Card_detail
{
    private String id;

    private Object bank_id;

    private String updated_at;

    private String card_number;

    private String payment_mode;

    private String created_at;

    private String payment_id;

    private String bin_id;

    private String card_type_id;

    private String name_on_card;

    private String country;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public Object getBank_id ()
{
    return bank_id;
}

    public void setBank_id (Object bank_id)
    {
        this.bank_id = bank_id;
    }

    public String getUpdated_at ()
    {
        return updated_at;
    }

    public void setUpdated_at (String updated_at)
    {
        this.updated_at = updated_at;
    }

    public String getCard_number ()
    {
        return card_number;
    }

    public void setCard_number (String card_number)
    {
        this.card_number = card_number;
    }

    public String getPayment_mode ()
    {
        return payment_mode;
    }

    public void setPayment_mode (String payment_mode)
    {
        this.payment_mode = payment_mode;
    }

    public String getCreated_at ()
    {
        return created_at;
    }

    public void setCreated_at (String created_at)
    {
        this.created_at = created_at;
    }

    public String getPayment_id ()
    {
        return payment_id;
    }

    public void setPayment_id (String payment_id)
    {
        this.payment_id = payment_id;
    }

    public String getBin_id ()
    {
        return bin_id;
    }

    public void setBin_id (String bin_id)
    {
        this.bin_id = bin_id;
    }

    public String getCard_type_id ()
    {
        return card_type_id;
    }

    public void setCard_type_id (String card_type_id)
    {
        this.card_type_id = card_type_id;
    }

    public String getName_on_card ()
    {
        return name_on_card;
    }

    public void setName_on_card (String name_on_card)
    {
        this.name_on_card = name_on_card;
    }

    public String getCountry ()
    {
        return country;
    }

    public void setCountry (String country)
    {
        this.country = country;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", bank_id = "+bank_id+", updated_at = "+updated_at+", card_number = "+card_number+", payment_mode = "+payment_mode+", created_at = "+created_at+", payment_id = "+payment_id+", bin_id = "+bin_id+", card_type_id = "+card_type_id+", name_on_card = "+name_on_card+", country = "+country+"]";
    }
}

