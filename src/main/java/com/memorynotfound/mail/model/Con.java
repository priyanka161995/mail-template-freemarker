package com.memorynotfound.mail.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Con
{
    private String port;

    private Object verify_callback;

    private String curr_http_version;

    private String open_timeout;

    private Object debug_output;

    private Object ciphers;

    private String sspi_enabled;

    private Object ca_file;

    private Object cert;

    private String enable_post_connection_check;

    private String close_on_empty_response;

    private Object socket;

    private Object compression;

    private Object key;

    private Object verify_depth;

    private Object verify_mode;

    private Object ca_path;

    private Object ssl_version;

    private String use_ssl;

    private Object ssl_context;

    private Object cert_store;

    private String read_timeout;

    private String address;

    private Object ssl_timeout;

    private String started;

    private Object continue_timeout;

    private String no_keepalive_server;

    public String getPort ()
    {
        return port;
    }

    public void setPort (String port)
    {
        this.port = port;
    }

    public Object getVerify_callback ()
{
    return verify_callback;
}

    public void setVerify_callback (Object verify_callback)
    {
        this.verify_callback = verify_callback;
    }

    public String getCurr_http_version ()
    {
        return curr_http_version;
    }

    public void setCurr_http_version (String curr_http_version)
    {
        this.curr_http_version = curr_http_version;
    }

    public String getOpen_timeout ()
{
    return open_timeout;
}

    public void setOpen_timeout (String open_timeout)
    {
        this.open_timeout = open_timeout;
    }

    public Object getDebug_output ()
{
    return debug_output;
}

    public void setDebug_output (Object debug_output)
    {
        this.debug_output = debug_output;
    }

    public Object getCiphers ()
{
    return ciphers;
}

    public void setCiphers (Object ciphers)
    {
        this.ciphers = ciphers;
    }

    public String getSspi_enabled ()
    {
        return sspi_enabled;
    }

    public void setSspi_enabled (String sspi_enabled)
    {
        this.sspi_enabled = sspi_enabled;
    }

    public Object getCa_file ()
{
    return ca_file;
}

    public void setCa_file (Object ca_file)
    {
        this.ca_file = ca_file;
    }

    public Object getCert ()
{
    return cert;
}

    public void setCert (Object cert)
    {
        this.cert = cert;
    }

    public String getEnable_post_connection_check ()
    {
        return enable_post_connection_check;
    }

    public void setEnable_post_connection_check (String enable_post_connection_check)
    {
        this.enable_post_connection_check = enable_post_connection_check;
    }

    public String getClose_on_empty_response ()
    {
        return close_on_empty_response;
    }

    public void setClose_on_empty_response (String close_on_empty_response)
    {
        this.close_on_empty_response = close_on_empty_response;
    }

    public Object getSocket ()
{
    return socket;
}

    public void setSocket (Object socket)
    {
        this.socket = socket;
    }

    public Object getCompression ()
{
    return compression;
}

    public void setCompression (Object compression)
    {
        this.compression = compression;
    }

    public Object getKey ()
{
    return key;
}

    public void setKey (Object key)
    {
        this.key = key;
    }

    public Object getVerify_depth ()
{
    return verify_depth;
}

    public void setVerify_depth (Object verify_depth)
    {
        this.verify_depth = verify_depth;
    }

    public Object getVerify_mode ()
{
    return verify_mode;
}

    public void setVerify_mode (Object verify_mode)
    {
        this.verify_mode = verify_mode;
    }

    public Object getCa_path ()
{
    return ca_path;
}

    public void setCa_path (Object ca_path)
    {
        this.ca_path = ca_path;
    }

    public Object getSsl_version ()
{
    return ssl_version;
}

    public void setSsl_version (Object ssl_version)
    {
        this.ssl_version = ssl_version;
    }

    public String getUse_ssl ()
    {
        return use_ssl;
    }

    public void setUse_ssl (String use_ssl)
    {
        this.use_ssl = use_ssl;
    }

    public Object getSsl_context ()
{
    return ssl_context;
}

    public void setSsl_context (Object ssl_context)
    {
        this.ssl_context = ssl_context;
    }

    public Object getCert_store ()
{
    return cert_store;
}

    public void setCert_store (Object cert_store)
    {
        this.cert_store = cert_store;
    }

    public String getRead_timeout ()
    {
        return read_timeout;
    }

    public void setRead_timeout (String read_timeout)
    {
        this.read_timeout = read_timeout;
    }

    public String getAddress ()
    {
        return address;
    }

    public void setAddress (String address)
    {
        this.address = address;
    }

    public Object getSsl_timeout ()
{
    return ssl_timeout;
}

    public void setSsl_timeout (Object ssl_timeout)
    {
        this.ssl_timeout = ssl_timeout;
    }

    public String getStarted ()
    {
        return started;
    }

    public void setStarted (String started)
    {
        this.started = started;
    }

    public Object getContinue_timeout ()
{
    return continue_timeout;
}

    public void setContinue_timeout (Object continue_timeout)
    {
        this.continue_timeout = continue_timeout;
    }

    public String getNo_keepalive_server ()
    {
        return no_keepalive_server;
    }

    public void setNo_keepalive_server (String no_keepalive_server)
    {
        this.no_keepalive_server = no_keepalive_server;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [port = "+port+", verify_callback = "+verify_callback+", curr_http_version = "+curr_http_version+", open_timeout = "+open_timeout+", debug_output = "+debug_output+", ciphers = "+ciphers+", sspi_enabled = "+sspi_enabled+", ca_file = "+ca_file+", cert = "+cert+", enable_post_connection_check = "+enable_post_connection_check+", close_on_empty_response = "+close_on_empty_response+", socket = "+socket+", compression = "+compression+", key = "+key+", verify_depth = "+verify_depth+", verify_mode = "+verify_mode+", ca_path = "+ca_path+", ssl_version = "+ssl_version+", use_ssl = "+use_ssl+", ssl_context = "+ssl_context+", cert_store = "+cert_store+", read_timeout = "+read_timeout+", address = "+address+", ssl_timeout = "+ssl_timeout+", started = "+started+", continue_timeout = "+continue_timeout+", no_keepalive_server = "+no_keepalive_server+"]";
    }
}

