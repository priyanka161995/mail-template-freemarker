package com.memorynotfound.mail.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Refunds
{
    private String refund_file_id;

    private String rollbacked;

    private String status;

    private String rollback_supported;

    private String bos_file_id;

    private String pbos_file_id;

    private String arn_no;

    private String attempted;

    private String id;

    private String payment_reconciliation_file_id;

    private String updated_at;

    private String seq_no;

    private String description;

    private String txn_id;

    private String refund_type;

    private String created_at;

    private String pg_generation_txn_id;

    private String taken;

    private String payment_id;

    private String refund_amount;

    public String getRefund_file_id ()
{
    return refund_file_id;
}

    public void setRefund_file_id (String refund_file_id)
    {
        this.refund_file_id = refund_file_id;
    }

    public String getRollbacked ()
{
    return rollbacked;
}

    public void setRollbacked (String rollbacked)
    {
        this.rollbacked = rollbacked;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getRollback_supported ()
{
    return rollback_supported;
}

    public void setRollback_supported (String rollback_supported)
    {
        this.rollback_supported = rollback_supported;
    }

    public String getBos_file_id ()
{
    return bos_file_id;
}

    public void setBos_file_id (String bos_file_id)
    {
        this.bos_file_id = bos_file_id;
    }

    public String getPbos_file_id ()
{
    return pbos_file_id;
}

    public void setPbos_file_id (String pbos_file_id)
    {
        this.pbos_file_id = pbos_file_id;
    }

    public String getArn_no ()
{
    return arn_no;
}

    public void setArn_no (String arn_no)
    {
        this.arn_no = arn_no;
    }

    public String getAttempted ()
{
    return attempted;
}

    public void setAttempted (String attempted)
    {
        this.attempted = attempted;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getPayment_reconciliation_file_id ()
{
    return payment_reconciliation_file_id;
}

    public void setPayment_reconciliation_file_id (String payment_reconciliation_file_id)
    {
        this.payment_reconciliation_file_id = payment_reconciliation_file_id;
    }

    public String getUpdated_at ()
    {
        return updated_at;
    }

    public void setUpdated_at (String updated_at)
    {
        this.updated_at = updated_at;
    }

    public String getSeq_no ()
{
    return seq_no;
}

    public void setSeq_no (String seq_no)
    {
        this.seq_no = seq_no;
    }

    public String getDescription ()
{
    return description;
}

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getTxn_id ()
    {
        return txn_id;
    }

    public void setTxn_id (String txn_id)
    {
        this.txn_id = txn_id;
    }

    public String getRefund_type ()
    {
        return refund_type;
    }

    public void setRefund_type (String refund_type)
    {
        this.refund_type = refund_type;
    }

    public String getCreated_at ()
    {
        return created_at;
    }

    public void setCreated_at (String created_at)
    {
        this.created_at = created_at;
    }

    public String getPg_generation_txn_id ()
{
    return pg_generation_txn_id;
}

    public void setPg_generation_txn_id (String pg_generation_txn_id)
    {
        this.pg_generation_txn_id = pg_generation_txn_id;
    }

    public String getTaken ()
{
    return taken;
}

    public void setTaken (String taken)
    {
        this.taken = taken;
    }

    public String getPayment_id ()
    {
        return payment_id;
    }

    public void setPayment_id (String payment_id)
    {
        this.payment_id = payment_id;
    }

    public String getRefund_amount ()
    {
        return refund_amount;
    }

    public void setRefund_amount (String refund_amount)
    {
        this.refund_amount = refund_amount;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [refund_file_id = "+refund_file_id+", rollbacked = "+rollbacked+", status = "+status+", rollback_supported = "+rollback_supported+", bos_file_id = "+bos_file_id+", pbos_file_id = "+pbos_file_id+", arn_no = "+arn_no+", attempted = "+attempted+", id = "+id+", payment_reconciliation_file_id = "+payment_reconciliation_file_id+", updated_at = "+updated_at+", seq_no = "+seq_no+", description = "+description+", txn_id = "+txn_id+", refund_type = "+refund_type+", created_at = "+created_at+", pg_generation_txn_id = "+pg_generation_txn_id+", taken = "+taken+", payment_id = "+payment_id+", refund_amount = "+refund_amount+"]";
    }
}

