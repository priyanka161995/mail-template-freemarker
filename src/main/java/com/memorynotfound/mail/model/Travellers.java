package com.memorynotfound.mail.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Travellers
{
    private String ADT;

    public String getADT ()
    {
        return ADT;
    }

    public void setADT (String ADT)
    {
        this.ADT = ADT;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [ADT = "+ADT+"]";
    }
}

