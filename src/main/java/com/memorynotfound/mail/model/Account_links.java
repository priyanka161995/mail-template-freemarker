package com.memorynotfound.mail.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Account_links
{
    private String cancel_page;

    private String ticket_page;

    private String invoice_page;

    private String trip_details_page;

    public String getCancel_page ()
    {
        return cancel_page;
    }

    public void setCancel_page (String cancel_page)
    {
        this.cancel_page = cancel_page;
    }

    public String getTicket_page ()
    {
        return ticket_page;
    }

    public void setTicket_page (String ticket_page)
    {
        this.ticket_page = ticket_page;
    }

    public String getInvoice_page ()
    {
        return invoice_page;
    }

    public void setInvoice_page (String invoice_page)
    {
        this.invoice_page = invoice_page;
    }

    public String getTrip_details_page ()
    {
        return trip_details_page;
    }

    public void setTrip_details_page (String trip_details_page)
    {
        this.trip_details_page = trip_details_page;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [cancel_page = "+cancel_page+", ticket_page = "+ticket_page+", invoice_page = "+invoice_page+", trip_details_page = "+trip_details_page+"]";
    }
}

