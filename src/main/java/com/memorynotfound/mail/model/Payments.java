package com.memorynotfound.mail.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Payments
{
    private String linkable_id;

    private String payment_subtype;

    private String order_info1;

    private String order_info2;

    private String currency;

    private String id;

    private String amount;

    private String app_ref1;

    private Card_detail card_detail;

    private String app_ref2;

    private String seq_no;

    private String description;

    private String txn_id;

    private String created_at;

    private String emi_count;

    private String ref_payment_id;

    private String emi_fee;

    private String merchant_txn_ref;

    private String neglist_id;

    private String status;

    private String linkable_type;

    private String pan_number;

    private String poslist_id;

    private String trip_id;

    private String user_message;

    private String express_checkout;

    private String updated_at;

    private Refunds[] refunds;

    private String payment_category;

    private String payment_type;

    public String getLinkable_id ()
{
    return linkable_id;
}

    public void setLinkable_id (String linkable_id)
    {
        this.linkable_id = linkable_id;
    }

    public String getPayment_subtype ()
{
    return payment_subtype;
}

    public void setPayment_subtype (String payment_subtype)
    {
        this.payment_subtype = payment_subtype;
    }

    public String getOrder_info1 ()
    {
        return order_info1;
    }

    public void setOrder_info1 (String order_info1)
    {
        this.order_info1 = order_info1;
    }

    public String getOrder_info2 ()
    {
        return order_info2;
    }

    public void setOrder_info2 (String order_info2)
    {
        this.order_info2 = order_info2;
    }

    public String getCurrency ()
    {
        return currency;
    }

    public void setCurrency (String currency)
    {
        this.currency = currency;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getAmount ()
    {
        return amount;
    }

    public void setAmount (String amount)
    {
        this.amount = amount;
    }

    public String getApp_ref1 ()
    {
        return app_ref1;
    }

    public void setApp_ref1 (String app_ref1)
    {
        this.app_ref1 = app_ref1;
    }

    public Card_detail getCard_detail ()
    {
        return card_detail;
    }

    public void setCard_detail (Card_detail card_detail)
    {
        this.card_detail = card_detail;
    }

    public String getApp_ref2 ()
    {
        return app_ref2;
    }

    public void setApp_ref2 (String app_ref2)
    {
        this.app_ref2 = app_ref2;
    }

    public String getSeq_no ()
    {
        return seq_no;
    }

    public void setSeq_no (String seq_no)
    {
        this.seq_no = seq_no;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getTxn_id ()
    {
        return txn_id;
    }

    public void setTxn_id (String txn_id)
    {
        this.txn_id = txn_id;
    }

    public String getCreated_at ()
    {
        return created_at;
    }

    public void setCreated_at (String created_at)
    {
        this.created_at = created_at;
    }

    public String getEmi_count ()
{
    return emi_count;
}

    public void setEmi_count (String emi_count)
    {
        this.emi_count = emi_count;
    }

    public String getRef_payment_id ()
{
    return ref_payment_id;
}

    public void setRef_payment_id (String ref_payment_id)
    {
        this.ref_payment_id = ref_payment_id;
    }

    public String getEmi_fee ()
{
    return emi_fee;
}

    public void setEmi_fee (String emi_fee)
    {
        this.emi_fee = emi_fee;
    }

    public String getMerchant_txn_ref ()
    {
        return merchant_txn_ref;
    }

    public void setMerchant_txn_ref (String merchant_txn_ref)
    {
        this.merchant_txn_ref = merchant_txn_ref;
    }

    public String getNeglist_id ()
{
    return neglist_id;
}

    public void setNeglist_id (String neglist_id)
    {
        this.neglist_id = neglist_id;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getLinkable_type ()
{
    return linkable_type;
}

    public void setLinkable_type (String linkable_type)
    {
        this.linkable_type = linkable_type;
    }

    public String getPan_number ()
{
    return pan_number;
}

    public void setPan_number (String pan_number)
    {
        this.pan_number = pan_number;
    }

    public String getPoslist_id ()
{
    return poslist_id;
}

    public void setPoslist_id (String poslist_id)
    {
        this.poslist_id = poslist_id;
    }

    public String getTrip_id ()
    {
        return trip_id;
    }

    public void setTrip_id (String trip_id)
    {
        this.trip_id = trip_id;
    }

    public String getUser_message ()
    {
        return user_message;
    }

    public void setUser_message (String user_message)
    {
        this.user_message = user_message;
    }

    public String getExpress_checkout ()
{
    return express_checkout;
}

    public void setExpress_checkout (String express_checkout)
    {
        this.express_checkout = express_checkout;
    }

    public String getUpdated_at ()
    {
        return updated_at;
    }

    public void setUpdated_at (String updated_at)
    {
        this.updated_at = updated_at;
    }

    public Refunds[] getRefunds ()
    {
        return refunds;
    }

    public void setRefunds (Refunds[] refunds)
    {
        this.refunds = refunds;
    }

    public String getPayment_category ()
    {
        return payment_category;
    }

    public void setPayment_category (String payment_category)
    {
        this.payment_category = payment_category;
    }

    public String getPayment_type ()
    {
        return payment_type;
    }

    public void setPayment_type (String payment_type)
    {
        this.payment_type = payment_type;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [linkable_id = "+linkable_id+", payment_subtype = "+payment_subtype+", order_info1 = "+order_info1+", order_info2 = "+order_info2+", currency = "+currency+", id = "+id+", amount = "+amount+", app_ref1 = "+app_ref1+", card_detail = "+card_detail+", app_ref2 = "+app_ref2+", seq_no = "+seq_no+", description = "+description+", txn_id = "+txn_id+", created_at = "+created_at+", emi_count = "+emi_count+", ref_payment_id = "+ref_payment_id+", emi_fee = "+emi_fee+", merchant_txn_ref = "+merchant_txn_ref+", neglist_id = "+neglist_id+", status = "+status+", linkable_type = "+linkable_type+", pan_number = "+pan_number+", poslist_id = "+poslist_id+", trip_id = "+trip_id+", user_message = "+user_message+", express_checkout = "+express_checkout+", updated_at = "+updated_at+", refunds = "+refunds+", payment_category = "+payment_category+", payment_type = "+payment_type+"]";
    }
}

