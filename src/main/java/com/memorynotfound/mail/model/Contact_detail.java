package com.memorynotfound.mail.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Contact_detail
{
    private String city_name;

    private String user_type;

    private String country_id;

    private String pin_code;

    private String state_name;

    private String country_code;

    private String country_name;

    private String trip_id;

    private String city_id;

    private String id;

    private String state_id;

    private String first_name;

    private String landline;

    private String title;

    private String updated_at;

    private String mobile_number;

    private String address;

    private String address_id;

    private String email;

    private String last_name;

    private String created_at;

    private String mobile;

    public String getCity_name ()
{
    return city_name;
}

    public void setCity_name (String city_name)
    {
        this.city_name = city_name;
    }

    public String getUser_type ()
{
    return user_type;
}

    public void setUser_type (String user_type)
    {
        this.user_type = user_type;
    }

    public String getCountry_id ()
{
    return country_id;
}

    public void setCountry_id (String country_id)
    {
        this.country_id = country_id;
    }

    public String getPin_code ()
{
    return pin_code;
}

    public void setPin_code (String pin_code)
    {
        this.pin_code = pin_code;
    }

    public String getState_name ()
{
    return state_name;
}

    public void setState_name (String state_name)
    {
        this.state_name = state_name;
    }

    public String getCountry_code ()
{
    return country_code;
}

    public void setCountry_code (String country_code)
    {
        this.country_code = country_code;
    }

    public String getCountry_name ()
{
    return country_name;
}

    public void setCountry_name (String country_name)
    {
        this.country_name = country_name;
    }

    public String getTrip_id ()
    {
        return trip_id;
    }

    public void setTrip_id (String trip_id)
    {
        this.trip_id = trip_id;
    }

    public String getCity_id ()
{
    return city_id;
}

    public void setCity_id (String city_id)
    {
        this.city_id = city_id;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getState_id ()
{
    return state_id;
}

    public void setState_id (String state_id)
    {
        this.state_id = state_id;
    }

    public String getFirst_name ()
    {
        return first_name;
    }

    public void setFirst_name (String first_name)
    {
        this.first_name = first_name;
    }

    public String getLandline ()
{
    return landline;
}

    public void setLandline (String landline)
    {
        this.landline = landline;
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public String getUpdated_at ()
    {
        return updated_at;
    }

    public void setUpdated_at (String updated_at)
    {
        this.updated_at = updated_at;
    }

    public String getMobile_number ()
    {
        return mobile_number;
    }

    public void setMobile_number (String mobile_number)
    {
        this.mobile_number = mobile_number;
    }

    public String getAddress ()
{
    return address;
}

    public void setAddress (String address)
    {
        this.address = address;
    }

    public String getAddress_id ()
{
    return address_id;
}

    public void setAddress_id (String address_id)
    {
        this.address_id = address_id;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getLast_name ()
    {
        return last_name;
    }

    public void setLast_name (String last_name)
    {
        this.last_name = last_name;
    }

    public String getCreated_at ()
    {
        return created_at;
    }

    public void setCreated_at (String created_at)
    {
        this.created_at = created_at;
    }

    public String getMobile ()
    {
        return mobile;
    }

    public void setMobile (String mobile)
    {
        this.mobile = mobile;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [city_name = "+city_name+", user_type = "+user_type+", country_id = "+country_id+", pin_code = "+pin_code+", state_name = "+state_name+", country_code = "+country_code+", country_name = "+country_name+", trip_id = "+trip_id+", city_id = "+city_id+", id = "+id+", state_id = "+state_id+", first_name = "+first_name+", landline = "+landline+", title = "+title+", updated_at = "+updated_at+", mobile_number = "+mobile_number+", address = "+address+", address_id = "+address_id+", email = "+email+", last_name = "+last_name+", created_at = "+created_at+", mobile = "+mobile+"]";
    }
}

