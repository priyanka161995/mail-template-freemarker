package com.memorynotfound.mail;

import com.memorynotfound.mail.model.MyPojo;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Map;
import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import org.springframework.web.util.HtmlUtils;

@Service
public class EmailService {

    @Autowired
    private JavaMailSender emailSender;

    @Autowired
    @Qualifier("tem")
    private Configuration freemarkerConfig;

    public void sendSimpleMessage(Mail mail) throws MessagingException, IOException, TemplateException {
        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message,
                MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                StandardCharsets.UTF_8.name());

        helper.addAttachment("logo.png", new ClassPathResource("memorynotfound-logo.png"));

        BufferedReader reader = new BufferedReader(new FileReader("/home/priyankakhatri/mail-template-freemaker/src/main/resources/templates/confirmation_train.ftl"));
        //BufferedReader reader = new BufferedReader(new FileReader("/home/kulasekarreddy/mail-template-freemaker/src/main/resources/templates/train.ftl"));
        StringBuilder stringBuilder = new StringBuilder();
        String line = null;
        String ls = System.getProperty("line.separator");
        while ((line = reader.readLine()) != null) {
            stringBuilder.append(line);
            stringBuilder.append(ls);
        }
        // delete the last new line separator
        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        reader.close();

        String content = stringBuilder.toString();
        content = StringEscapeUtils.unescapeHtml4(content);
        //Template t = freemarkerConfig.getTemplate("train.ftl");
        //String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, mail.getModel());
        String template = content.replaceAll("&amp;lt;", "<").replaceAll("&amp;gt;",">");
        //String template = content;
        //System.out.println(template);
        //Map<String, Object> model = mail.getModel();
        MyPojo myPojo=mail.getMyPojo();
        //System.out.println(myPojo);
        Configuration cfg = new Configuration();
        cfg.setObjectWrapper(new DefaultObjectWrapper());

        Template t = new Template("templateName", new StringReader(template), cfg);

        Writer out = new StringWriter();
        t.process(myPojo, out);

        String html = out.toString();
        System.out.println(html);

        helper.setTo(mail.getTo());
        helper.setText(html, true);
        helper.setSubject(mail.getSubject());
        helper.setFrom(mail.getFrom());

        emailSender.send(message);
    }

}
