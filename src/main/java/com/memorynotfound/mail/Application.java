package com.memorynotfound.mail;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.memorynotfound.mail.model.MyPojo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


@SpringBootApplication
public class Application implements ApplicationRunner {

    private static Logger log = LoggerFactory.getLogger(Application.class);

    @Autowired
    private EmailService emailService;

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        log.info("Sending Email with Freemarker HTML Template Example");

        Mail mail = new Mail();
        mail.setFrom("priyanka.khatri@cleartrip.com");
        mail.setTo("priyanka.khatri@cleartrip.com");
        mail.setSubject("Sending Email with Freemarker HTML Template Example");

        Map<String, Object> model = new HashMap<String, Object>();
        MyPojo myPojo=new MyPojo();
        try {

            ObjectMapper mapper = new ObjectMapper();

           String json="{\"payment_info\":{\"pricing_info\":[[\"Base Fare\",\"Rs.  1,982\"],[\"Railway Charges\",\"Rs.  199\"],[\"Agent Service Charges\",\"Rs.  40\"],[\"Processing Fee\",\"Rs.  22\"]],\"payment_type_name\":\"Card#\",\"payment_type_id\":\"xxxx xxxx xxxx 2346\",\"logo\":\"mastercard\",\"payment_subtype\":null,\"logo_text\":null,\"payment_type\":\"Credit Card\",\"header\":\"\"},\"confirmation_subject\":\"Confirmed ticket for Ahmedabad - Lonavala one-way -- Trip ID Q1805231146\",\"trip_ref\":\"Q1805231146\",\"trip_booking_status\":\"Q\",\"firstname\":\"Test\",\"trip_name\":\"Ahmedabad - Lonavala one-way\",\"hostname\":\"development.cleartrip.com\",\"trip_fully_confirmed\":false,\"trip_unconfirmed_status\":false,\"trip_has_failed_parts\":false,\"trip_convert_failure_to_success_activated\":false,\"trip_train_booking_exists\":true,\"train_booking_info_exists\":true,\"amt_currency\":\"Rs.  2,247\",\"segments\":[{\"train_name\":\"Pune Duronto\",\"train_number\":\"12297\",\"departure_city_name\":null,\"arrival_city_name\":null,\"arrival_date_time\":\"2018-05-30T06:05:00+05:30\",\"duration\":27300.0,\"departure_date_time\":\"2018-05-29T22:30:00+05:30\"}],\"amendment\":false,\"trip_details_page\":\"http://cleartrip.com/account/trips/Q1805231146\",\"cleartrip_cust_support_no\":\"(+91) 95 95 333 333\",\"google_card_enabled\":false,\"total\":2242.85,\"format_total\":\"Rs.  2,243\",\"payment_type_key\":\"CC\",\"has_free_cancellation\":true,\"banner_image_prefix\":\"https://www.cleartrip.com\",\"country_code\":\"IN\",\"banner_file_name\":\"/Transaction_B2C_IN.json\",\"relative_file_path\":\"/usr/local/storage/appsdata/common/bannermgmtFiles/Transaction_B2C_IN.json\",\"hashBannerData\":{},\"banner_link\":\"https://www.cleartrip.com/eadserver/delivery/ck.php?zoneid=90\",\"banner_image\":\"https://www.cleartrip.com/eadserver/delivery/avw.php?zoneid=90 &amp;cb=78535\",\"trip_corp_booking\":false,\"trip_b2c_booking\":true,\"user\":41646312,\"p\":{},\"p_confirmed_cleartrip_user\":false,\"p_express_checkout_enabled\":null,\"hide_button\":false,\"activation_url\":\"https://development.cleartrip.com/register\",\"upsells\":[\"Cab\",\"Expressway\"],\"train_booking_infos\":[{\"confirmation_status\":\"PQWL\",\"seat_number_with_coach\":\"Waitlist - \",\"pax_info\":{\"age\":25,\"title\":\"Ms\",\"first_name\":\"test\",\"last_name\":\"test\",\"pax_type_code\":\"ADT\"}}],\"pnr_number\":\"8205868031\",\"today_date\":2018,\"train_confirmation_status_rac\":\"RAC\",\"ticket_attached\":false,\"mime_partial\":true,\"from\":\"no-reply@cleartrip.com\",\"logo\":\"/home/priyankakhatri/rails/chronicle_r3/public/mailer/images/global/cleartrip_logo_emails.gif\",\"subject\":\"Confirmed ticket for Ahmedabad - Lonavala one-way -- Trip ID Q1805231146\",\"recipients\":[\"priyanka.khatri@cleartrip.com\"],\"show_train_marketing_banner\":false,\"from_address\":\"Cleartrip Bookings <no-reply@cleartrip.com>\"}";

            myPojo = mapper.readValue(json, new TypeReference<MyPojo>(){});

            System.out.println(model);

        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

//        model.put("train_name", "Chamundi Expres");
//        model.put("train_number", "train_number");
//        model.put("pnr", "pnr");
//        model.put("username", "username Expres");
//        model.put("trip_id", "trip_id");
//        model.put("source", "source");
//        model.put("destination", "destination Expres");
//        model.put("train_start_date", "train_start_date");
//        model.put("duration", "2h 45m");
//        model.put("dep_time", "12:45");
//        model.put("arr_time", "21");
//
//
//        model.put("card_number", "card_number");
//        model.put("payment_mode", "Debit Card");
//        model.put("total_fare", "100");
//        model.put("basic_fare", "90");
//        model.put("railway_charges", "20");
//        model.put("ct_service_fee", "15");
//        model.put("processing_fee", "1");
//
//        Map<String, Object> traveller1 = new HashMap<String, Object>();
//        traveller1.put("name", "name1");
//        traveller1.put("info", "26yrs Adult");
//        traveller1.put("status", "Confirmed");
//        traveller1.put("seat_no", "D38");
//
//        Map<String, Object> traveller2 = new HashMap<String, Object>();
//        traveller2.put("name", "name2");
//        traveller2.put("info", "26yrs Adult");
//        traveller2.put("status", "Confirmed");
//        traveller2.put("seat_no", "D39");
//
//        List<Map> travellers = new ArrayList<>();
//        travellers.add(traveller1);
//        travellers.add(traveller2);
//        model.put("travellers", travellers);
//
//        System.out.println(model);
       // mail.setModel(model);
        mail.setMyPojo(myPojo);
        emailService.sendSimpleMessage(mail);
    }
}
