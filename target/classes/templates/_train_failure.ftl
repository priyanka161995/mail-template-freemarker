<tr>

  <td style="font-size:22px;color:#000000;font-weight:bold;" bgcolor="#ffffff">

    <h1 style="color:#000000; padding-left:20px; padding-top:0px; font-size:24px;">

      <img height="25px" width="191px" src="https://${hostname}/mailers/images/train_confirmed_title.png" alt="Ticket attached"/>

    </h1>

  </td>

  <td style="text-align:right; padding-right:20px; padding-top:15px" bgcolor="#ffffff">

    <h2>

      <img width="137px" height="45px" alt="Cleartrip" src="https://${hostname}/mailers/mages/cleartrip_logo.png"/>

    </h2>

  </td>

</tr>

<tr>

  <td colspan="2" style="padding:0 30px 20px 25px;font-size:18px;color:#000000;">

      <p>We're very sorry, but we were unable to confirm your ${trip.trip_name}  trip and you have been charged = <#if amt_currency??> ${amt_currency}</#if> . Please do not panic &ndash; We are trying to confirm your booking. If the confirmation does not happen, we will refund the amount you have paid. Please call us in the next 15 minutes to know the latest on your booking. For any assistance please call: = ${cleartrip_cust_support_no} (standard STD / local charges apply).</p>

  </td>

</tr>

