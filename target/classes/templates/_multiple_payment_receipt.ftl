
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:center;padding:30px 10px 20px 10px;">

  <tbody>

    <tr>

      <td style="font-size:10px;text-transform:uppercase;color:#999999;">Payment Receipt</td>

    </tr>

    

    <#if (total_payment_types == 2 && !nb??) || total_payment_types == 1>

    <tr>

      <td style="padding-bottom:5px;">

	<table table border="0" cellpadding="0" cellspacing="0" width="100%">

	  <tbody>

	    <tr>

	    <#assign i=0>
	     <#list payment_types as a >

	      <td>

	     <#if a?values?first["logo_text"]?? >

	      <span style="font-size: 18px;font-weight: regular;color: #000000;text-align: center;">${a.values.first["logo_text"]}</span>

	    <#elseif a?values?first["logo"] >

		  <img src="https://${hostname}/images/mailers/= a.values.first["logo"] _logo.png" style="padding:10px 0;font-size:18px;color:#000000;" alt=${a?keys?first}>

		 <#else> 

		<span style="font-size:18px;font-weight:bold;text-transform:uppercase;color:#000000;">${a?keys?first}</span>

		</#if> 

	      </td>

	      <#if i < total_payment_types-1 && total_payment_types > 1>

	      <td style="text-align:left;color:#999999;padding-right:5px;width:2%;padding-bottom:5px;">

			<span>+</span>

	      </td>

	      </#if>
	      <#assign i=i+1>
	      </#list>

	    </tr>

	  </tbody>

	</table>

      </td>

    </tr>

    <#elseif total_payment_types == 2 && nb??>

    <tr>

      <td style="padding-bottom:5px;">

	<table table border="0" cellpadding="0" cellspacing="0" width="100%">

	  <tbody>

	    <tr>

	      <td>

    	<#if nb["logo"]??>

		<img src="https://${hostname}/images/mailers/${$nb["logo"]}_logo.png" style="padding:10px 0;font-size:18px;color:#000000;" alt="${nb['payment_type']}">

		 <#else> 

		<span style="font-size:18px;font-weight:bold;text-transform:uppercase;color:#000000;">${nb['payment_type']}

		</span>

		</#if>

	      </td>

	    </tr>

	  </tbody>

	</table>

      </td>

    </tr>

    <tr>

      <td style="padding-bottom:5px;">

	<table table border="0" cellpadding="0" cellspacing="0" width="100%">

	  <tbody>

	    <tr>

	      <td>

		<div style="color:#999999;font-size:10px;text-transform:uppercase;margin:10px 0 0px 0;">Netbanking through</div>

		<div style="color:#000000;font-size:18px;margin:0px;font-weight:bold;">${nb["payment_type_id"]} Bank</div>	 

	      </td>

	    </tr>

	    </tbody>

	</table>

      </td>

      

    </tr>

  <#elseif total_payment_types == 3 && !nb??>

    <tr>

      <td style="padding-bottom:5px;">

	<table table border="0" cellpadding="0" cellspacing="0" width="100%">

	  <tbody>

	    <tr>


	    
	    <#assign i = 0 >

	      <#list payment_types as a>

	      <td>

	     <#if a?values?first["logo_text"]?? >

	      <span style="font-size: 18px;font-weight: regular;color: #000000;text-align: center;">${a?values?first["logo_text"]}</span>

	    <#elseif a?values?first["logo"] >

		<img src="https://${hostname}/images/mailers/{a?values?first["logo"]}_logo.png" style="padding:10px 0;font-size:18px;color:#000000;" alt="${a?keys?first}">

		 <#else> 

		<span style="font-size:18px;font-weight:bold;text-transform:uppercase;color:#000000;">${a?keys?first}</span>

		</#if> 

	      </td>

	      <#if i == 0>

	      <td style="text-align:left;color:#999999;padding-right:5px;width:2%;padding-bottom:5px;">

		<span>+</span>

	      </td>

	      </#if>
	      <#assign i=i+1>
	      </#list>

	  </tbody>

	</table>

      </td>

    </tr>

    

    <#elseif total_payment_types == 3 && !nb??>

    <tr>

      <td>

	<table>

	  <tbody>

	  <tr>

	  	<#assign i=0>
		 <#list payment_types_selection.each_with_index as a>

	    

	    <td style="padding-bottom:5px;">

       <#if a?values?first["logo_text"]?? >

        <span style="font-size: 18px;font-weight: regular;color: #000000;text-align: center;">${a.values.first["logo_text"]}</span>

      <#elseif a?values?first["logo"]?? >

	      <img src="https://${hostname}/images/mailers/= a.values.first["logo"]_logo.png" style="padding:10px 0;font-size:18px;color:#000000;" alt="${a?keys?first}">

	       <#else> 

	      <span style="font-size:18px;font-weight:bold;text-transform:uppercase;color:#000000;">${a?keys?first}</span>

	      </#if> 

	    </td>

	    <#if i == 0>

	    <td style="text-align:left;color:#999999;padding-right:5px;width:2%;padding-bottom:5px;">

	      <span>+</span>

	    </td>

	    </#if>

	    </#list>

	  </tr>

	  <tbody>

	</table>

      </td>

    </tr>

    <tr>

      <td style="padding-bottom:5px;">

	<table table border="0" cellpadding="0" cellspacing="0" width="100%">

	  <tbody>

	    <tr>

	      <td>

		<div style="color:#999999;font-size:10px;text-transform:uppercase;margin:10px 0 0px 0;">Netbanking through</div>

		<div style="color:#000000;font-size:18px;margin:0px;font-weight:bold;">${nb["payment_type_id"]} Bank</div>	 

	      </td>

	    </tr>

	    </tbody>

	</table>

      </td>

      

    </tr>

    

  <#else>
  <#if total_payment_types == 1>

  <tr>

    <td style="padding-bottom:5px;">

     <#if a?values?first["logo_text"]?? >

      <span style="font-size: 18px;font-weight: regular;color: #000000;text-align: center;">${a?values?first["logo_text"]}</span>

    <#elseif a?values?first["logo"] >

    <img src="https://${hostname}/images/mailers/${a?values?first['logo']}_logo.png" style="padding:10px 0;font-size:18px;color:#000000;" alt="${a?keys?first}">

     <#else> 

    <span style="font-size:18px;font-weight:bold;text-transform:uppercase;color:#000000;">${a?keys?first}</span>

    </#if> 

    </td>

  </tr>
  </#if>
  </#if>

  

  <tr>

    <td style="border-top:1px solid #eeeeee;padding-bottom:10px;">

      <div style="color:#999999;font-size:10px;text-transform:uppercase;margin:10px 0 0px 0;">Total charge</div>

      <div style="color:#000000;font-size:18px;margin:0px;font-weight:bold;">${format_total}</div>

    </td>

  </tr>

  <tr>

    <td style="border-top:1px solid #eeeeee;">

      <div style="color:#999999;font-size:10px;text-transform:uppercase;margin:10px 0 0px 0;">Fare breakup</div>

      <div style="color:#000000;font-size:18px;margin:10px 0px;font-size:13px;">

	

	<table width="100%" border="0" cellspacing="0" cellpadding="0">

	  

<#list payment_info["pricing_info"]  as   pricing_info>
          <tr>

            <td style="text-align:right;color:#999999;vertical-align:top;padding-right:5px;width:50%;padding-bottom:5px;">${pricing_info[0]}</td>

            <td style="text-align:left;color:#000000;vertical-align:top;padding-left:5px;width:50%;padding-bottom:5px;">${pricing_info[1]}</td> <#--- rounded value to be done -->

          </tr>

          </#list> 

	   <#if additional_breakup>

<#list additional_breakup as   add_bearkup>
	  <tr>

            <td style="text-align:right;color:#999999;vertical-align:top;padding-right:5px;width:50%;padding-bottom:5px;">${add_bearkup[0]}</td>

            <td style="text-align:left;color:#000000;vertical-align:top;padding-left:5px;width:50%;padding-bottom:5px;">${add_bearkup[1]}</td> <#--add_bearkup[1]= ${GeneralHelpers::format_currency(add_bearkup[1].abs.round, 0 ,currency)} -->

          </tr>

	  </#list> 

	  </#if> 

          <tr>

            <td style="text-align:right;color:#999999;vertical-align:top;padding-right:5px;padding-bottom:5px;">Total</td>

            <td style="text-align:left;color:#000000;vertical-align:top;padding-left:5px;padding-bottom:5px;">${format_total}</td> <#-- format total to round -->

          </tr>

      

	</table>

      </div>

    </td>

  </tr>

  <#if !payment_info["payments"]?? && payment_info["payments"].length > 1 >

  <tr>

    <td style="border-top:1px solid #eeeeee;">

      <div style="color:#999999;font-size:10px;text-transform:uppercase;margin:10px 0 0px 0;">Payment breakup</div>

      <div style="color:#000000;font-size:18px;margin:10px 0px;font-size:13px;">

	<table width="100%" border="0" cellspacing="0" cellpadding="0">

	  <tbody>

    <#list payment_types as p>

       paid_type = (p.values.first['payment_subtype"]"?? ? p?values?first["payment_subtype"] : p?keys?first) 

	    <tr>

	      <td style="text-align:right;color:#999999;vertical-align:top;padding-right:5px;width:50%;padding-bottom:5px;">By ${paid_type}</td>

	      <td style="text-align:left;color:#000000;vertical-align:top;padding-left:5px;width:50%;padding-bottom:5px;">${p?values?first['format_amount']}</td> <#-- ${GeneralHelpers::format_currency(p?values?first['amount'], 0, currency)} -->

	    </tr>

	    </#list>

	    

	</tbody></table>

      </div>

    </td>

  </tr>

</#if>

</tbody>

</table>

