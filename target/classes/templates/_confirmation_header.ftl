<head>

  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

  <title>${confirmation_subject}</title>

  <style>

    body {font-family:Arial, Verdana,sans-serif; margin:0; padding:0;}

    h1, h2, h3, h4, h5, h6 { margin:0; padding:0;}

    a { color:#3366cc; outline:none; border:none}

    a img { outline:none; border:none;}

  </style>

</head>
