<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<#include "src/main/resources/templates/_confirmation_header.ftl">


<body style="font-family:Arial, Verdana,sans-serif; margin:0; padding:0;"> <body style="background-color: #666666; padding-top: 10px; padding-bottom: 10px;">

<#include "src/main/resources/templates/_google_now_cards.ftl">
<div style="background:#cccccc; margin:0; width:100%">

  <div style="margin:0px auto; width:575px; padding:30px 0 0px 0;"> 

    <p style="text-align:center;"><img src="https://${hostname}/images/mailers/thankyou_top.gif" style="text-align:center; padding:10px 0 0 0; font-size:24px;color:#9f9f9f;" width="437px" height="38px" alt="Thank you for booking with Cleartrip"></p>

    <div style="height:10px;-webkit-border-radius:10px 10px 0 0;border-radius:10px 10px 0 0;background-color:#ffffff;"></div>

    <div style="background-color:#ffffff;">

      <table width="100%" border="0" cellspacing="0" cellpadding="0">

          <tr>

             <#if trip_fully_confirmed || trip.booking_status == "Q" || trip.booking_status == "K" || (trip_has_failed_parts && trip_convert_failure_to_success_activated)  >

               <#if trip_train_booking_exists >

<#include "src/main/resources/templates/_train_confirmation.ftl">
               <#else> 

<#include "src/main/resources/templates/_train_failure.ftl">
              </#if> 

            </#if> 

          </tr>

          <tr>

            <td style="padding:10px 10px 0px 25px;vertical-align:top;"><table width="100%" border="0" cellpadding="0" cellspacing="0"><tbody>

<#include "src/main/resources/templates/_train_details.ftl">
<#include "src/main/resources/templates/_train_traveller_details.ftl">
            </tbody></table></td>

	          <#if payment_info["payments"]??>

	              <td width="254px;" height="392px;" background="http://${hostname}/images/slip.png" style="vertical-align:top;background-image:url(http://${hostname}/images/slip.png);background-repeat:no-repeat;padding-right:30px;">

	          <#else>

              <td style="vertical-align:top;padding-right:10px;width:272px" width="272px">

	          </#if>

<#include "src/main/resources/templates/_payment_receipt_confirmation.ftl">
            </td>

          </tr>

<#include "src/main/resources/templates/_confirmation_deal_block.ftl">
           <#if (show_train_marketing_banner)?? >

            <tr>

              <td colspan="2" style="text-align:center;background-color:#f4f4f4;">

                <a href="https://www.surveymonkey.com/s/558CSQN"><img src="https://${hostname}/images/mailers/train_marketing_banner.jpg" alt="Redeem your cash back" height="60px;" width="428px;"></a>

              </td>

             </tr>

          </#if> 

          <tr>

            <td style="background-color:#f4f4f4;padding:20px 25px 10px 25px;text-align:left;letter-spacing:-0.2px;" colspan="2">

<#include "src/main/resources/templates/_upsells.ftl">
            </td>

          </tr>

        </table>

      </div>

<#include "src/main/resources/templates/_confirmation_footer.ftl">
    </div>

  </div>

</body>

</html>

